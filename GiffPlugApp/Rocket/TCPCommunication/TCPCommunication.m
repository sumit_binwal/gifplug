
//
//  TCPCommunication.m
//  TCPWebserviceConsumption
//
//  Created by Ghanshyam on 08/07/14.
//  Copyright (c) 2014 Ghanshyam. All rights reserved.
//

#import "TCPCommunication.h"
#import "NSData+Base64.h"
#import "AppDelegate.h"
#import "Config.h"
//On 04/02/16, at 4:16 PM, cmd wrote:
//> for image upload of chat use
//202.157.76.19:8008

//For live media share... upload
//#define TCP_SERVER_IP @"202.157.76.19"

//#define TCP_SERVER_IP_DEVELOPMENT @"202.157.76.19"
//#define TCP_SERVER_PORT_DEVELOPMENT 8355
//
//#define TCP_SERVER_IP_CLIENT @"202.157.76.19"
//#define TCP_SERVER_PORT_CLIENT 8356

//For live media share... download
//#define TCP_SERVER_IP @"202.157.76.19"
//#define TCP_SERVER_PORT 8005

@implementation TCPCommunication

-(id)initWithBackgroundQueue:(dispatch_queue_t)queue{
    self = [super init];
    if (self) {
        self.backgroundQueue = queue; //dispatch_queue_create("com.kipl.thread", NULL);
        containerData = [[NSMutableData alloc] init];
    }
    return self;
}

#pragma mark--
#pragma mark-- Communication Method

/**
 @discussion Used to initiate TCP connection and configure Input & Output Streams .
 Input stream used to get response from server and Output Stream used
 to write data over server
 */

-(void)setUpCommunicationStream{
   // dispatch_queue_t backgroundThread = dispatch_queue_create("com.kipl.thread", NULL);
    
//    dispatch_async(dispatch_get_main_queue(), ^{
        if (_inputStream && _outputStream) {
            //already TCP Connection is existing
            NSLog(@"streams are existing");
            return;
        }
        //NSLog(@"new streams are created");
        CFReadStreamRef  readStream;
        CFWriteStreamRef writeStream;
        CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)TCP_SERVER_IP,TCP_SERVER_PORT, &readStream, &writeStream);
    
        //CFWriteStreamCreateWithFTPURL(<#CFAllocatorRef alloc#>, <#CFURLRef ftpURL#>)
        // Indicate that we want socket to be closed whenever streams are closed.
        CFReadStreamSetProperty(readStream, kCFStreamPropertyShouldCloseNativeSocket,
                            kCFBooleanTrue);
        CFWriteStreamSetProperty(writeStream, kCFStreamPropertyShouldCloseNativeSocket,
                             kCFBooleanTrue);
    
        //Setting Up Stream Object for Communication with App
        self.inputStream  = (__bridge_transfer NSInputStream *)readStream;
        [_inputStream setDelegate:self];
        [_inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        [_inputStream open];
    
        self.outputStream = (__bridge_transfer NSOutputStream *)writeStream;
        [_outputStream setDelegate:self];
        [_outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        [_outputStream open];
    
        _checkIndex = 0;
    
//        [[NSRunLoop currentRunLoop] run];
//        CFRunLoopRun();
//    });
}

- (void)keepaliveWith:(NSStream *)theStream
{
    NSData *dataa = (NSData *)[theStream propertyForKey:(__bridge NSString *)kCFStreamPropertySocketNativeHandle];
    
    if(dataa)
    {
        CFSocketNativeHandle socket_handle = *(CFSocketNativeHandle *)[dataa bytes];
        //NSLog(@"SOCK HANDLE: %x", socket_handle);
        
        //SO_KEEPALIVE option to activate
        int option = 1;
//        //TCP_NODELAY option to activate
//        int option2 = 1;
//        //Idle time used when SO_KEEPALIVE is enabled. Sets how long connection must be idle before keepalive is sent
//        int keepaliveIdle = 10;
//        //Interval between keepalives when there is no reply. Not same as idle time
//        int keepaliveIntvl = 2;
//        //Number of keepalives before close (including first keepalive packet)
//        int keepaliveCount = 4;
//        //Time after which tcp packet retransmissions will be stopped and the connection will be dropped.Stream is closed
//        int retransmissionTimeout = 5;
        
        
        if (setsockopt(socket_handle, SOL_SOCKET, SO_KEEPALIVE, &option, sizeof (int)) == -1)
        {
            NSLog(@"setsockopt SO_KEEPALIVE failed: %s", strerror(errno));
        }else
        {
            NSLog(@"setsockopt SO_KEEPALIVE ok");
        }
        
//        if (setsockopt(socket_handle, IPPROTO_TCP, TCP_KEEPCNT, &keepaliveCount, sizeof(int)) == -1)
//        {
//            NSLog(@"setsockopt TCP_KEEPCNT failed: %s", strerror(errno));
//        }else
//        {
//            NSLog(@"setsockopt TCP_KEEPCNT ok");
//        }
//        
//        if (setsockopt(socket_handle, IPPROTO_TCP, TCP_KEEPALIVE, &keepaliveIdle, sizeof(int)) == -1)
//        {
//            NSLog(@"setsockopt TCP_KEEPALIVE failed: %s", strerror(errno));
//        }else
//        {
//            NSLog(@"setsockopt TCP_KEEPALIVE ok");
//        }
//        
//        if (setsockopt(socket_handle, IPPROTO_TCP, TCP_KEEPINTVL, &keepaliveIntvl, sizeof(int)) == -1)
//        {
//            NSLog(@"setsockopt TCP_KEEPINTVL failed: %s", strerror(errno));
//        }else
//        {
//            NSLog(@"setsockopt TCP_KEEPINTVL ok");
//        }
//        
//        if (setsockopt(socket_handle, IPPROTO_TCP, TCP_RXT_CONNDROPTIME, &retransmissionTimeout, sizeof(int)) == -1)
//        {
//            NSLog(@"setsockopt TCP_RXT_CONNDROPTIME failed: %s", strerror(errno));
//        }else
//        {
//            NSLog(@"setsockopt TCP_RXT_CONNDROPTIME ok");
//        }
//        
//        if (setsockopt(socket_handle, IPPROTO_TCP, TCP_NODELAY, &option2, sizeof(int)) == -1)
//        {
//            NSLog(@"setsockopt TCP_NODELAY failed: %s", strerror(errno));
//        }else
//        {
//            NSLog(@"setsockopt TCP_NODELAY ok");
//        }
    }
}
#pragma mark--
#pragma mark-- Stream Delegate
-(void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    @try {
        if (aStream == _inputStream) {
            switch (eventCode) {
                case NSStreamEventNone:
                    //NSLog(@"server is not up");
                    break;
                case NSStreamEventErrorOccurred:
                    NSLog(@"input stream event error");
                    [self closeStream];
//                    isSocketConnected = NO;
//                     [self setUpCommunicationStream];
                    break;
                case NSStreamEventOpenCompleted:
                    isSocketConnected = YES;
                    NSLog(@"input stream created successfully");
//                    [self keepaliveWith:aStream];
                    break;
                case NSStreamEventHasBytesAvailable:
                    if (!data) {
                        data = [[NSMutableData alloc] init];
                    }
                    uint8_t buffer[10240];
                    NSInteger bufferSize;
                    
                    while ([_inputStream hasBytesAvailable]) {
                        bufferSize = [_inputStream read:buffer maxLength:sizeof(buffer)];
                        

                        NSLog(@"buffer size %ld",(long)bufferSize);
                        
                        
                        if (bufferSize>0)
                        {
                            NSString *output = [[NSString alloc] initWithBytes:buffer length:bufferSize encoding:NSASCIIStringEncoding];
                        //   NSLog(@"output is %@",output);
                            if ([output rangeOfString:@"\n"].location != NSNotFound) {
                                NSLog(@"newline found");
                            }
                            if ([output rangeOfString:@"\n"].location == NSNotFound ) {
                                if (containerData) {
                                    [containerData appendBytes:(const void *)buffer length:bufferSize];
                                    //below two lines comeented.
                                    
                                  NSString *response = [[NSString alloc] initWithData:containerData encoding:NSUTF8StringEncoding];
                                    
                                   NSLog(@"response is \n%@",response);
                                }
                             }else{
                                int lineCount = (int)[[output componentsSeparatedByString:@"\n"] count];
                                NSData *newData = nil;
                                for (int counter = 0; counter<(lineCount-1); counter++) {
                                    NSString *dataContent = [[output componentsSeparatedByString:@"\n"] objectAtIndex:counter];
                                    NSData *tmpData1 = [dataContent dataUsingEncoding:NSUTF8StringEncoding];
                                    if (containerData) {
                                        [containerData appendData:tmpData1];
                                        newData = [NSData dataWithData:containerData];
                                       NSString *response = [[NSString alloc] initWithData:newData encoding:NSUTF8StringEncoding];
                                        
                                       NSLog(@"iska response is \n%@",response);
                                        @synchronized(self){
                                            
                                            [self dataReceived:newData];
                                        }
                                        if (containerData) {
                                            [containerData setLength:0];
                                        }
                                    }
                                }
                                NSString *nextString = [[output componentsSeparatedByString:@"\n"] objectAtIndex:lineCount-1];
                                NSData *tmpData2 = [nextString dataUsingEncoding:NSUTF8StringEncoding];
                                if (containerData) {
                                    [containerData appendData:tmpData2];
                                }
                                
                            }
                        }else if (bufferSize<0){
                            NSLog(@"breaking end of stream");
                            break;
                        }
                    }
                    break;
                case NSStreamEventEndEncountered:
                    NSLog(@"input stream event end encountered");
                    [self closeStream];
//                    isSocketConnected = NO;
//                    [self setUpCommunicationStream];
                    break;
                default:
                    break;
            }
        }else if (aStream == _outputStream){
            switch (eventCode) {
                case NSStreamEventErrorOccurred:
                    NSLog(@"output stream error occurred");
                    [self closeStream];
//                    isSocketConnected = NO;
//                    [self setUpCommunicationStream];
                    break;
                case NSStreamEventHasSpaceAvailable:
                    processing= NO;
                    [self processingPendingRequest];
                    NSLog(@"outputstream space available to write");
                    break;
                case NSStreamEventOpenCompleted:
                    isSocketConnected = YES;
                    [APPDELEGATE connectToSocket];
                    NSLog(@"outputstream stream openend successfully");
                    break;
                case NSStreamEventEndEncountered:
                    NSLog(@"output stream event end occurred");
                    [self closeStream];
//                    isSocketConnected = NO;
//                    [self setUpCommunicationStream];
                    break;
                default:
                    break;
            }
        }
    }
    @catch (NSException *exception) {
        //NSLog(@"tcp communication exception ");
    }
    @finally {
        
    }
}

#pragma mark--
#pragma mark-- Custom Method
-(void)closeStream{
   
    NSLog(@"going to close connection");
    
    if (_inputStream) {
        //NSLog(@"inside inputstream");
        _inputStream.delegate = self;
        [_inputStream close];
        [_inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        self.inputStream = nil;
    }
    if (_outputStream) {
        //NSLog(@"inside outputstream");
        _outputStream.delegate = self;
        [_outputStream close];
        [_outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        self.outputStream = nil;
    }
    
    if ([_delegate conformsToProtocol:@protocol(TCPDelegate)] &&
        [_delegate respondsToSelector:@selector(connectionClosed)]) {
        //NSLog(@"connection closed delegate");
        [_delegate connectionClosed];
    }
    CFRunLoopStop(CFRunLoopGetCurrent());
}

-(void)reconnect{
    //NSLog(@"reconnected to server");
    [_inputStream open];
    [_outputStream open];
}
-(void)closeStreamWithZero
{
    [_inputStream close];
    [_outputStream close];
}

-(void)dataReceived:(NSData *)responseData
{
    NSData *dataVal = [NSData dataWithData:responseData];
    //[data setLength:0];
    
//    NSString *response = [[NSString alloc] initWithData:dataVal encoding:NSUTF8StringEncoding];
    
   // NSLog(@"response is \n%@",response);
    
    [self processingPendingRequest];
    if ([_delegate conformsToProtocol:@protocol(TCPDelegate)] &&
        [_delegate respondsToSelector:@selector(dataReceived:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate dataReceived:dataVal];
        });
    }
    
}

-(void)processingPendingRequest
{
    if (_lastRequestImage) {
        [self sendRequestImageData:_lastRequestImage];
    }
    if (_lastRequest) {
        NSLog(@"pending request");
        [self sendRequest:_lastRequest];
    
   }
}

//-(void)sendRequest:(NSString *)request{
//    if (_outputStream && [_outputStream hasSpaceAvailable] && processing == NO) {
//        NSLog(@"processed request is%@",request);
//        self.lastRequest = nil;
//        self.lastRequestImage = nil;
//        processing = YES;
//         NSData *requestData = [[NSData alloc] initWithData:[request dataUsingEncoding:NSUTF8StringEncoding]];
//       // NSData *data = UIImageJPEGRepresentation(drawImage.image, 90);
//    
//       
//        
////        NSData *requestData = [[NSData alloc] initWithData:[request dataUsingEncoding:NSUTF8StringEncoding]];
//   [_outputStream write:[requestData bytes] maxLength:[requestData length]];
////        
////        int index = 0;
////        int totalLen =(int) [requestData length];
////        NSLog(@"total len is %d",totalLen);
////        uint8_t buffer[1024];
////        uint8_t *readBytes = (uint8_t *)[requestData bytes];
////        
////        while (index < totalLen) {
////            if (_outputStream && [_outputStream hasSpaceAvailable]) {
////                int indexLen =  (1024>(totalLen-index))?(totalLen-index):1024;
////                
////               // (void)memcpy(buffer, readBytes, indexLen);
////                
////                int written =(int) [_outputStream write:buffer maxLength:indexLen];
////               NSLog(@"Wrote %d bytes to stream %@.",written, _outputStream);
////                if (written < 0) {
////                    break;
////                }
////                index += written;
////                readBytes += written;
////                NSLog(@"index is %d",index);
////            }
////            
////        }
//        
//        if ([_delegate conformsToProtocol:@protocol(TCPDelegate)]
//            &&[_delegate respondsToSelector:@selector(requestSent)]) {
//            
//            
//            
//            [_delegate requestSent];
//        }
//        
//      //  [self closeStreamWithZero];
//    }else{
//        
//        NSLog(@"yaha se request gayi hai na %@",[_outputStream hasSpaceAvailable]?@"yes":@"no");
//        
//        self.lastRequest = request;
//    }
//}

-(void)sendRequest:(NSString *)request{
    
    if (_outputStream && [_outputStream hasSpaceAvailable] && processing == NO) {
        NSLog(@"processed request is%@",request);
        self.lastRequest = nil;
        self.lastRequestImage = nil;
        processing = YES;
        NSData *requestData = [[NSData alloc] initWithData:[request dataUsingEncoding:NSUTF8StringEncoding]];
        int index = 0;
        int totalLen =(int) [requestData length];
        NSLog(@"total len is %d",totalLen);
        uint8_t buffer[1024];
        uint8_t *readBytes = (uint8_t *)[requestData bytes];
        
        while (index < totalLen) {
            if (_outputStream && [_outputStream hasSpaceAvailable]) {
                int indexLen =  (1024>(totalLen-index))?(totalLen-index):1024;
                
                (void)memcpy(buffer, readBytes, indexLen);
                
                int written =(int) [_outputStream write:buffer maxLength:indexLen];
                NSLog(@"written is is %d",written);
                if (written < 0) {
                    break;
                }
                index += written;
                readBytes += written;
                NSLog(@"index is %d",index);
            }
            
        }
        
        if ([_delegate conformsToProtocol:@protocol(TCPDelegate)]
            &&[_delegate respondsToSelector:@selector(requestSent)]) {
            
            [_delegate requestSent];
        }
        
        //  [self closeStreamWithZero];
    }else{
        
        NSLog(@"yaha se request gayi hai na %@",[_outputStream hasSpaceAvailable]?@"yes":@"no");
        
        self.lastRequest = request;
    }
}

-(void)sendRequestImageData:(NSString *)request
{
    if (_outputStream && [_outputStream hasSpaceAvailable]&& processing == NO)
    {
        self.lastRequest = nil;
        self.lastRequestImage = nil;
        processing = YES;
       // NSData *newData = UIImageJPEGRepresentation([UIImage imageNamed:@"Tutorial04"], 1.0);
       // NSString *finalImagePath = [newData base64EncodedString];
        
        //NSString *strImageData = [finalImagePath stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        //NSLog(@"str image data %@",finalImagePath);
        NSData* data1 = [request dataUsingEncoding:NSUTF8StringEncoding];
        int index = 0;
        int totalLen =(int) [data1 length];
        NSLog(@"total len is %d",totalLen);
        uint8_t buffer[1024];
        uint8_t *readBytes = (uint8_t *)[data1 bytes];
        
        while (index < totalLen) {
            if (_outputStream && [_outputStream hasSpaceAvailable]) {
                int indexLen =  (1024>(totalLen-index))?(totalLen-index):1024;
                
                (void)memcpy(buffer, readBytes, indexLen);
                
                int written =(int) [_outputStream write:buffer maxLength:indexLen];
                NSLog(@"written is is %d",written);
                if (written < 0) {
                    break;
                }
                index += written;
                readBytes += written;
                NSLog(@"index is %d",index);
            }
        }
       // [self closeStreamWithZero];
    }
    else{
        self.lastRequestImage = request;
    }
}

-(void)connectionClosed{
    NSLog(@"connectionClosed");
}

-(void)requestSent{
    NSLog(@"requestSent");
}

-(void)dealloc{
    
    //NSLog(@"tcp dealloc");
//    [self closeStream];
//    containerData = nil;
//    data = nil;
//    self.backgroundQueue = nil;
}

-(void)releaseData{
    containerData = nil;
    data = nil;
}

@end
