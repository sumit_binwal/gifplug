//
//  CategoryDetailVC.m
//  GiffPlugApp
//
//  Created by Santosh on 04/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "CategoryDetailVC.h"
#import "AfterSearchTableCell.h"
#import "GifCollectionCell.h"
#import "SinglePostVC.h"

@interface CategoryDetailVC ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *topGifsArray;
    NSMutableArray *recentGifsArray;
    
    NSInteger currentPage, totalPage;
    BOOL isPageRefreshing;
    UIRefreshControl *refreshPullDown;
}
@property (strong, nonatomic) IBOutlet UIView *loaderFooter;
@property (weak, nonatomic) IBOutlet UIImageView *loaderImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPosts;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)onBack:(id)sender;
@end

@implementation CategoryDetailVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    topGifsArray = [[NSMutableArray alloc]init];
    recentGifsArray = [[NSMutableArray alloc]init];
    self.tableViewPosts.estimatedRowHeight = 44.0f;
    self.tableViewPosts.hidden = YES;
    
    currentPage =1;
    totalPage = 0;
    isPageRefreshing = YES;
    
    self.titleLabel.text = self.categoryName;
    
    refreshPullDown = [[UIRefreshControl alloc]initWithFrame:CGRectMake(0, 0, self.tableViewPosts.frame.size.width, 50)];
    refreshPullDown.backgroundColor=[UIColor blackColor];
    refreshPullDown.tintColor=[UIColor whiteColor];
    [refreshPullDown addTarget:self action:@selector(onRefreshPullDown) forControlEvents:UIControlEventValueChanged];
    [self.tableViewPosts addSubview:refreshPullDown];
    
    [self getCategoryPosts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)onRefreshPullDown
{
    if ([CommonFunction reachabiltyCheck]) {
        currentPage = 1;
        totalPage = 0;
        isPageRefreshing = YES;
        [self getCategoryPosts];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        [refreshPullDown endRefreshing];
    }
}

#pragma  mark - WebService API

-(void)getCategoryPosts
{
    [CommonFunction showActivityIndicatorWithText:@""];
    NSString *url = [NSString stringWithFormat:@"posts/getPostInCategory/%@/%ld",self.categoryTag,(long)currentPage];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self.tableViewPosts setTableFooterView:nil];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        
        if (refreshPullDown.isRefreshing)
        {
            AudioServicesPlaySystemSound(1109);
        }
        [refreshPullDown endRefreshing];
        
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
        }
        else if([operation.response statusCode]==200)
        {
            totalPage=[[responseDict objectForKey:@"total_pages"] integerValue];
            if ([responseDict[@"recent_posts"] isKindOfClass:[NSArray class]])
            {
                NSArray *recentPostArray = responseDict[@"recent_posts"];
//                if (recentPostArray.count>0)
//                {
                    if (currentPage == 1)
                    {
                        [recentGifsArray removeAllObjects];
                    }
                    [recentGifsArray addObjectsFromArray:[recentPostArray mutableCopy]];
//                }
            }
            
            if ([responseDict[@"top_posts"] isKindOfClass:[NSArray class]])
            {
                NSArray *topPostArray = responseDict[@"top_posts"];
//                if (topGifsArray.count>0)
//                {
                    [topGifsArray removeAllObjects];
//                }
                [topGifsArray addObjectsFromArray:[topPostArray mutableCopy]];
            }
            isPageRefreshing = NO;
            
            weakSelf.tableViewPosts.hidden = NO;
            [self.tableViewPosts reloadData];
        }
        else
        {
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [refreshPullDown endRefreshing];
                                          [self.tableViewPosts setTableFooterView:nil];
                                          if (currentPage>1)
                                          {
                                              currentPage--;
                                          }
                                          isPageRefreshing = NO;
                                          
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}





#pragma mark -
#pragma mark UITableView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 || indexPath.row == 2)
    {
        return 54.0f * SCREEN_XScale;
    }
    return UITableViewAutomaticDimension;// Gif cell
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AfterSearchTableCell *cell = nil;
    switch (indexPath.row)
    {
        case 0:
        {
            
        }
            
        case 2:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:kCellAfterSearchIdentifierLabel];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AfterSearchTableCell class]) owner:self options:nil][1];
                cell.cellType = CELL_TYPE_LABEL;
                [cell adjustViewAccordingToCellType];
            }
            cell.cellType = CELL_TYPE_LABEL;
        }
            break;
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:kCellAfterSearchIdentifierGif];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AfterSearchTableCell class]) owner:self options:nil][2];
                cell.cellType = CELL_TYPE_COLLECTION_GIF;
                [cell adjustViewAccordingToCellType];
            }
            cell.cellType = CELL_TYPE_COLLECTION_GIF;
        }
            break;
        case 3:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:kCellAfterSearchIdentifierGifRecent];
            if (!cell)
            {
                cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([AfterSearchTableCell class]) owner:self options:nil][3];
                cell.cellType = CELL_TYPE_COLLECTION_GIF_RECENT;
                [cell adjustViewAccordingToCellType];
            }
            cell.cellType = CELL_TYPE_COLLECTION_GIF_RECENT;
        }
            break;
            
        default:
            break;
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(AfterSearchTableCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0:
        {
            cell.labelHeader.text = topGifsArray.count>0?@"TOP GIFS":@"TOP GIFS (0 Result)";
        }
            break;
        case 2:
        {
            cell.labelHeader.text = recentGifsArray.count>0?@"RECENT":@"RECENT (0 Result)";
        }
            break;
        case 1:
        {
            
        }
        case 3:
        {
            if (!cell.collectionView.delegate)
            {
                cell.collectionView.delegate = self;
                cell.collectionView.dataSource = self;
            }
            [cell.collectionView layoutIfNeeded];
            [cell.collectionView reloadData];
        }
            break;
            
        default:
            break;
    }
}



#pragma mark -
#pragma mark UICollectionView Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(104*SCREEN_XScale, 104*SCREEN_XScale);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(1,3,1,3);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == 102)
    {
        return topGifsArray.count;
    }
    return recentGifsArray.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 102)
    {
        GifCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellGifCollectionIdentifier forIndexPath:indexPath];
        
        __weak typeof(cell) weakCell = cell;
        
        cell.gifImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        NSString *imgGifUrl=topGifsArray[indexPath.row][@"post_image"];
        
        NSURL *url1 = [NSURL URLWithString:imgGifUrl];
        
        [cell.loader startAnimating];
        
        [cell.gifImageView sd_setImageWithURL:url1 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            weakCell.gifImageView.image=image;
            weakCell.animatingImage = image;
            weakCell.singleImage = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1)];
            [weakCell.loader stopAnimating];
            weakCell.loader.hidden = YES;
        }];
        
        return cell;
    }
    
    GifCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellGifCollectionIdentifier forIndexPath:indexPath];
    
    __weak typeof(cell) weakCell = cell;
    
    
    cell.gifImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    NSString *imgGifUrl=recentGifsArray[indexPath.row][@"post_image"];
    
    NSURL *url1 = [NSURL URLWithString:imgGifUrl];
    
    [cell.loader startAnimating];
    
    [cell.gifImageView sd_setImageWithURL:url1 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        weakCell.gifImageView.image=image;
        weakCell.animatingImage = image;
        weakCell.singleImage = [UIImage imageWithData:UIImageJPEGRepresentation(image, 1)];
        [weakCell.loader stopAnimating];
        weakCell.loader.hidden = YES;
    }];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *postID = nil;
    if (collectionView.tag == 102)
    {
        postID = topGifsArray[indexPath.row][@"post_id"];
    }
    else
    {
        postID = recentGifsArray[indexPath.row][@"post_id"];
    }
    SinglePostVC *postvc = [[SinglePostVC alloc]initWithNibName:NSStringFromClass([SinglePostVC class]) bundle:nil];
    postvc.postID = postID;
    postvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:postvc animated:YES];
}


#pragma mark - FooterView Animating
-(void)animateFooterView
{
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionRepeat|UIViewAnimationOptionCurveLinear
                     animations:^{
                         [_loaderImageView setTransform:CGAffineTransformRotate([_loaderImageView transform], M_PI-0.00001f)];
                     } completion:nil];
    
}

#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([self.tableViewPosts contentOffset].y + self.tableViewPosts.frame.size.height) >= [self.tableViewPosts contentSize].height)
    {
        NSLog(@"scrolled to bottom");
        if (!isPageRefreshing) {
            
            if(totalPage <= currentPage)
            {
            }
            else
            {
                currentPage++;
                isPageRefreshing = YES;
                [self animateFooterView];
                [self.tableViewPosts setTableFooterView:_loaderFooter];
                [self getCategoryPosts];
            }
        }
    }
    [self updateCellsOnScroll];
}


- (void)updateCellsOnScroll
{
    NSArray* cells = self.tableViewPosts.visibleCells;
    
    NSMutableArray *collectionArray = [NSMutableArray array];
    for (NSInteger i=0; i<cells.count; i++)
    {
        AfterSearchTableCell *cell = cells[i];
        if (cell.cellType == CELL_TYPE_COLLECTION_GIF_RECENT || cell.cellType == CELL_TYPE_COLLECTION_GIF)
        {
            [collectionArray addObject:cell.collectionView];
        }
    }
    
    for (NSInteger i=0; i<collectionArray.count; i++)
    {
        UICollectionView *collectionView = collectionArray[i];
        
        NSArray *visibleCells = collectionView.visibleCells;
        NSArray* indexPaths = collectionView.indexPathsForVisibleItems;
        
        NSUInteger cellCount = [visibleCells count];
        
        if (cellCount == 0) return;
        
        for (NSUInteger j = 0; j < cellCount; j++)
            [self checkVisibilityOfCell:[visibleCells objectAtIndex:j] forIndexPath:[indexPaths objectAtIndex:j]inCollection:collectionView];
    }
}

- (void)checkVisibilityOfCell:(GifCollectionCell *)cell forIndexPath:(NSIndexPath *)indexPath inCollection:(UICollectionView *)collectionView {
    UICollectionViewLayoutAttributes *attr = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
    CGRect cellRect = attr.frame;
    //    NSLog(@"*********** y position :%f",cellRect.origin.y);
    cellRect = [collectionView convertRect:cellRect toView:_tableViewPosts.superview];
    //    NSLog(@"*********** new y position :%f",cellRect.origin.y);
    BOOL completelyVisible = CGRectContainsRect(_tableViewPosts.frame, cellRect);
    
    [cell notifyCellVisibleWithIsCompletelyVisible:completelyVisible];
}

- (IBAction)onBack:(id)sender {
    [self.navigationController  popViewControllerAnimated:YES];
}
@end
