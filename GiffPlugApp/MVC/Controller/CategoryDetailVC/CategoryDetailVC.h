//
//  CategoryDetailVC.h
//  GiffPlugApp
//
//  Created by Santosh on 04/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryDetailVC : UIViewController
@property(nonatomic,strong)NSString *categoryTag;
@property(nonatomic,strong)NSString *categoryName;
@end
