//
//  VideoTrimmerVC.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 27/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoPlayerViewController.h"

@interface VideoTrimmerVC : UIViewController
- (IBAction)crossBtnPressed:(id)sender;
- (IBAction)nextBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *videoView;

@property(nonatomic,strong)NSString *videoPath;

@property(nonatomic,strong)VideoPlayerViewController *myPlayerViewController;

@end
