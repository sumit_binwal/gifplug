//
//  VideoTrimmerVC.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 27/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "VideoTrimmerVC.h"

@interface VideoTrimmerVC ()

@end

@implementation VideoTrimmerVC

#pragma mark - View Life cycle;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    //Hide Navigation
    
    [self setNavigationProperties];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.myPlayerViewController = nil;
    [super viewDidDisappear:animated];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadVideoInView];
}

#pragma mark - Navigation Properties

-(void)setNavigationProperties
{
    [CommonFunction hideNavigationBarFromController:self];
    
}

#pragma mark - hide status bar

- (BOOL)prefersStatusBarHidden
{
    
    return YES;
}

#pragma mark - Load Video

-(void)loadVideoInView
{
    VideoPlayerViewController *player = [[VideoPlayerViewController alloc] init];
    player.URL = [NSURL fileURLWithPath:_videoPath];
    player.view.frame = CGRectMake(0,0,_videoView.frame.size.width,_videoView.frame.size.height+1);
    [_videoView addSubview:player.view];
    self.myPlayerViewController = player;
    player = nil;

}

#pragma mark - Actions

- (IBAction)crossBtnPressed:(id)sender
{

    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextBtnPressed:(id)sender
{



}

#pragma mark - Memory Managment

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    self.myPlayerViewController = nil;

    // Dispose of any resources that can be recreated.
}
- (void)dealloc {
   
    self.myPlayerViewController = nil;
    
    //[super dealloc];
}


@end
