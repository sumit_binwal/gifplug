//
//  ActivitiesVC.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "ActivitiesVC.h"

#import "ActivityCell.h"

#import "CommonFunction.h"

#import "FollowUnfollowCustomeCell.h"

#import "AppDataManager.h"

#import "OtherUserProfileVC.h"

#import "SinglePostVC.h"

#import "WebViewVC.h"


@interface ActivitiesVC ()<UITableViewDelegate,UITableViewDataSource,CustomTableViewCellDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *arrayActivities;
    NSMutableArray *arrayRequests;
    
    BOOL isRequestViewOpen;
    
    NSInteger currentPage, requestLimit;
    BOOL isPageRefreshing;
    AppDataManager *dataManager;
    BOOL stopRequest;
    UIRefreshControl *refreshPullDown;
}
@property (weak, nonatomic) IBOutlet UIView *viewRequestContainer;
@property (weak, nonatomic) IBOutlet UILabel *labelRequestUser;
@property (weak, nonatomic) IBOutlet UITableView *tableRequestUsers;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightTableRequest;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableActivities;
@property (weak, nonatomic) IBOutlet UIImageView *userIconImageView;
@property (strong, nonatomic) IBOutlet UIView *vwFooterVw;
@property (weak, nonatomic) IBOutlet UIImageView *imgFooterCircle;
@property (weak, nonatomic) IBOutlet UILabel *labelErrorMessage;

@end

@implementation ActivitiesVC

- (void)dealloc
{
    NSLog(@"dealloced called for ACTIVITY VC...................");
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataManager = [AppDataManager sharedInstance];
    
    arrayActivities = [[NSMutableArray alloc]init];
    arrayRequests = [[NSMutableArray alloc]init];
    
    self.containerViewHeightConstraint.constant = [UIScreen mainScreen].bounds.size.width;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onExpandRequestArea:)];
    tap.numberOfTapsRequired=1;
    [self.viewRequestContainer addGestureRecognizer:tap];
    
    self.tableActivities.tableFooterView = [UIView new];
    self.tableRequestUsers.tableFooterView = [UIView new];
    
    self.labelRequestUser.font = [UIFont fontWithName:self.labelRequestUser.font.fontName size:self.labelRequestUser.font.pointSize*SCREEN_XScale];
    
    isRequestViewOpen = NO;
    
    refreshPullDown = [[UIRefreshControl alloc]initWithFrame:CGRectMake(0, 0, self.tableActivities.frame.size.width, 50)];
    refreshPullDown.backgroundColor=[UIColor colorWithRed:33.0f/255.0f green:31.0f/255.0f blue:31.0f/255.0f alpha:1];
    refreshPullDown.tintColor=[UIColor whiteColor];
    [refreshPullDown addTarget:self action:@selector(onRefreshPullDown) forControlEvents:UIControlEventValueChanged];
    [self.tableActivities addSubview:refreshPullDown];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    unReadCount = 0;
    if ([APPDELEGATE.appTabBarController.tabBar viewWithTag:1000])
    {
        [[APPDELEGATE.appTabBarController.tabBar viewWithTag:1000]removeFromSuperview];
    }
    stopRequest = NO;
    currentPage = 1;
    isPageRefreshing = YES;
    [self getActivityList];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onRefreshPullDown
{
    if ([CommonFunction reachabiltyCheck]) {
        stopRequest = NO;
        currentPage = 1;
        isPageRefreshing = YES;
        [self getActivityList];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        [refreshPullDown endRefreshing];
    }
}

#pragma mark -
#pragma mark UITableView Delegate & Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        return 71.0f;
    }

    NSString *activityType = arrayActivities[indexPath.row][@"activity_type"];
    if ([activityType isEqualToString:@"comment"])
    {
        return UITableViewAutomaticDimension;
    }
    if ([activityType isEqualToString:@"followRequest"]) {
        return 69.0f *SCREEN_XScale;
    }

    return 68.0f *SCREEN_XScale;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        return 71.0f;
    }
    return UITableViewAutomaticDimension;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 100)
    {
        return arrayRequests.count;
    }
    return arrayActivities.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        FollowUnfollowCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"activityFrdRequestCell"];
        if (!cell)
        {
            cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([FollowUnfollowCustomeCell class]) owner:self options:nil][2];
            cell.delegate=self;
        }
        
        FollowUnfollowCustomeCell *requestCell = (FollowUnfollowCustomeCell *)cell;
        requestCell.lblUsername.text = arrayRequests[indexPath.row][@"username"];
        requestCell.lblUserBio.text = arrayRequests[indexPath.row][@"bio"];
        
        NSString *strAvtarimg=[NSString stringWithFormat:@"%@",arrayRequests[indexPath.row][@"avatar_image"]];
        
        if (strAvtarimg.length>1) {
            
            [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error)
                {
                    cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                }
                else
                {
                    if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleAspectFill;
                    }
                    else
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                    }
                }
            }];
            
//            [requestCell.imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"]];
        }
        else
        {
            [cell.imageViewProfile setImage:[UIImage imageNamed:@"default.jpg"]];
//            [requestCell.imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
        }
        
        for (UIGestureRecognizer *gesture in cell.imageViewProfile.gestureRecognizers)
        {
            [cell.imageViewProfile removeGestureRecognizer:gesture];
        }
        
        UITapGestureRecognizer *tappGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileBtnCLicked:)];
        tappGesture.numberOfTapsRequired=1;
        [cell.imageViewProfile addGestureRecognizer:tappGesture];
        cell.imageViewProfile.tag = indexPath.row;
        
        requestCell.btnCancelRequest.tag = indexPath.row;
        [requestCell.btnCancelRequest addTarget:self action:@selector(onRequestCancel:) forControlEvents:UIControlEventTouchUpInside];
        
        requestCell.btnFollowUnfollow.tag=indexPath.row;
        [requestCell.btnFollowUnfollow addTarget:self action:@selector(onRequestAccept:) forControlEvents:UIControlEventTouchUpInside];
        
//        requestCell.imgProfileImg.tag=indexPath.row;
//        [requestCell.imgProfileImg addTarget:self action:@selector(profileBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
    }
    
    
    NSString *cellIdentifier = nil;
    NSInteger index;
    
    NSString *activityType = arrayActivities[indexPath.row][@"activity_type"];
    if ([activityType isEqualToString:@"comment"])
    {
        cellIdentifier = kCellIdentifierComment;
        index = 2;
    }
    else if ([activityType isEqualToString:@"like"])
    {
        cellIdentifier = kCellIdentifierLike;
        index = 1;
    }
    else if ([activityType isEqualToString:@"followRequest"])
    {
        cellIdentifier = kCellIdentifierFollow;
        index = 3;
    }
    else
    {
        cellIdentifier = kCellIdentifierReplug;
        index = 0;
    }
    
    ActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
    {
        cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([ActivityCell class]) owner:self options:nil][index];
    }
    
    
    cell.activityProfileView.tag=indexPath.row;
    cell.activityGifView.tag=indexPath.row;
    cell.activityFollowUnFollowBtn.tag=indexPath.row;
    
    [cell.activityFollowUnFollowBtn addTarget:self action:@selector(followUnfollowBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.activityProfileView.tag=indexPath.row;
    
    for (UIGestureRecognizer *gesture in cell.activityProfileView.gestureRecognizers)
    {
        [cell.activityProfileView removeGestureRecognizer:gesture];
    }
    
    for (UIGestureRecognizer *gesture in cell.activityGifView.gestureRecognizers)
    {
        [cell.activityGifView removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tapGeture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onProfileImageTap:)];
    tapGeture.numberOfTapsRequired = 1;
    [cell.activityProfileView addGestureRecognizer:tapGeture];
    
    UITapGestureRecognizer *tapGetureGif = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onGifImageTap:)];
    tapGetureGif.numberOfTapsRequired = 1;
    [cell.activityGifView addGestureRecognizer:tapGetureGif];
    
    
    NSString *strAvtarimg=[NSString stringWithFormat:@"%@",arrayActivities[indexPath.row][@"avatar_image"]];
    
    if (strAvtarimg.length>0) {
        
        [cell.activityProfileView sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error)
            {
                cell.activityProfileView.contentMode=UIViewContentModeScaleToFill;
            }
            else
            {
                if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
                {
                    cell.activityProfileView.contentMode=UIViewContentModeScaleAspectFill;
                }
                else
                {
                    cell.activityProfileView.contentMode=UIViewContentModeScaleToFill;
                }
            }
        }];
        
//        [cell.activityProfileView sd_setImageWithURL:[NSURL URLWithString:strAvtarimg]placeholderImage:[UIImage imageNamed:@"default.jpg"]];
    }
    else
    {
        [cell.activityProfileView setImage:[UIImage imageNamed:@"default.jpg"]];
    }
    
    NSString *strGifImage=[NSString stringWithFormat:@"%@",arrayActivities[indexPath.row][@"post_image"]];
    
    if (strGifImage.length>0) {
        [cell.activityGifView sd_setImageWithURL:[NSURL URLWithString:strGifImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }
    
    if (arrayActivities[indexPath.row][@"showDetail"])
    {
        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrayActivities[indexPath.row][@"user_id"]])
        {
            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrayActivities[indexPath.row][@"user_id"] ];
            BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
            if (requestAcess) {
                [cell.activityFollowUnFollowBtn setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                cell.activityFollowUnFollowBtn.hidden = NO;
                cell.labelRequested.hidden = YES;
            }
            else
            {
                cell.activityFollowUnFollowBtn.hidden = YES;
                cell.labelRequested.hidden = NO;
            }
        }
        else
        {
            BOOL isFollowing = [arrayActivities [indexPath.row][@"is_following"]boolValue];
            if (isFollowing)
            {
                BOOL isRequestAccepted = [arrayActivities[indexPath.row][@"request_status"] boolValue];
                if (isRequestAccepted) {
                    [cell.activityFollowUnFollowBtn setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                    cell.activityFollowUnFollowBtn.hidden = NO;
                    cell.labelRequested.hidden = YES;
                }
                else
                {
                    cell.activityFollowUnFollowBtn.hidden = YES;
                    cell.labelRequested.hidden = NO;
                }
            }
            else
            {
                cell.activityFollowUnFollowBtn.hidden = NO;
                cell.labelRequested.hidden = YES;
                [cell.activityFollowUnFollowBtn setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
            }
        }
    }
    else
    {
        cell.activityFollowUnFollowBtn.hidden = YES;
        cell.labelRequested.hidden = YES;
        cell.loader.hidden = YES;
    }
    
    cell.activityTimeStampLabel.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[arrayActivities[indexPath.row][@"date"] doubleValue]]];
    
    cell.activityUserName.text = arrayActivities[indexPath.row][@"username"];
    
//    NSString *activityType = arrayActivities[indexPath.row][@"activity_type"];
    if ([activityType isEqualToString:@"comment"])
    {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 5.0f;
        paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
        //    paragraphStyle.maximumLineHeight = 5.0f;
        
        NSString *string = [NSString stringWithFormat:@"%@",arrayActivities[indexPath.row][@"comment"]];
        NSDictionary *attributtes = @{
                                      NSParagraphStyleAttributeName : paragraphStyle,
                                      };
        cell.activityCommentLabel.attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributtes];
    }
    else if ([activityType isEqualToString:@"followRequest"])
    {
        cell.activityCommentLabel.text = [NSString stringWithFormat:@"%@ started following you",arrayActivities[indexPath.row][@"username"]];
    }
    
    
    return cell;
}


- (void)onExpandRequestArea:(UITapGestureRecognizer *)tapGesture
{
    if (isRequestViewOpen)
    {
        [self closeRequestViewWithAnimation];
    }
    else
    {
        [self openRequestViewWithAnimation];
    }
}

- (void)openRequestViewWithAnimation
{
    isRequestViewOpen = YES;
    [self.viewRequestContainer setBackgroundColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
    [self.labelRequestUser setTextColor:[UIColor whiteColor]];
    [self.userIconImageView setImage:[UIImage imageNamed:@"userWhiteActivity"]];
    
    [self.tableRequestUsers reloadData];
    self.constraintHeightTableRequest.constant = MIN(self.tableRequestUsers.contentSize.height, self.view.frame.size.height);
    
    [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.8 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)closeRequestViewWithAnimation
{
    isRequestViewOpen = NO;
    [self.viewRequestContainer setBackgroundColor:[UIColor whiteColor]];
    [self.labelRequestUser setTextColor:[UIColor colorWithRed:228/255.0f green:15/255.0f blue:73/255.0f alpha:1.0f]];
    [self.userIconImageView setImage:[UIImage imageNamed:@"userActivity"]];
    self.constraintHeightTableRequest.constant = 0;
    
    [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.8 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

-(void)followUnfollowBtnCLicked:(UIButton *)sender
{
    ActivityCell *tableViewCell = (ActivityCell *)sender.superview.superview;
    
    tableViewCell.activityFollowUnFollowBtn.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:arrayActivities[sender.tag][@"user_id"],@"user_id",arrayActivities[sender.tag][@"account_type"],@"account_type", nil];
    
    if ([[arrayActivities[sender.tag][@"is_following"]stringValue] isEqualToString:@"0"]) {
        [self postFollowersListAPIAtIndex:sender.tag WithObject:dataDict];
    }
    else
    {
        [self unFollowUserApiAtIndex:sender.tag WithObject:dataDict];
    }
    
}

- (void)onProfileImageTap:(UITapGestureRecognizer *)tapGesture
{
    NSInteger index = tapGesture.view.tag;
    OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
    oupvc.hidesBottomBarWhenPushed=YES;
    oupvc.strUserName=arrayActivities[index][@"username"];
    [self.navigationController pushViewController:oupvc animated:YES];
}

- (void)onGifImageTap:(UITapGestureRecognizer *)tapGesture
{
    NSInteger index = tapGesture.view.tag;
    SinglePostVC *postvc = [[SinglePostVC alloc]initWithNibName:NSStringFromClass([SinglePostVC class]) bundle:nil];
    postvc.postID = arrayActivities[index][@"post_id"];
    postvc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:postvc animated:YES];
}

- (void)profileBtnCLicked:(UITapGestureRecognizer *)button
{
    NSInteger index = button.view.tag;
    OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
    oupvc.hidesBottomBarWhenPushed=YES;
    oupvc.strUserName=arrayRequests[index][@"username"];
    [self.navigationController pushViewController:oupvc animated:YES];
}
- (void)onRequestAccept:(UIButton *)button
{
    [CommonFunction showActivityIndicatorWithText:@""];
    
    NSInteger index = button.tag;
    NSDictionary *dict = arrayRequests[index];
    NSString *userId = arrayRequests[index][@"user_id"];
    
    FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[self.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    tableViewCell.btnCancelRequest.hidden = YES;
    tableViewCell.btnFollowUnfollow.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    NSString *url = [NSString stringWithFormat:@"users/acceptFollowRequest/%@",userId];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
        
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePut withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSInteger newIndex = [arrayRequests indexOfObject:dict];
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:0]];
        
        tableViewCell.btnCancelRequest.hidden = NO;
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil){
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
//            FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:0]];
//            
//            tableViewCell.btnCancelRequest.hidden = NO;
//            tableViewCell.btnFollowUnfollow.hidden = NO;
//            tableViewCell.loader.hidden = YES;
//            [tableViewCell.loader stopAnimating];
        }
        else if([operation.response statusCode]==200)
        {
            [arrayRequests removeObject:dict];
            [weakSelf.tableRequestUsers beginUpdates];
            [weakSelf.tableRequestUsers deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:newIndex inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
            [weakSelf.tableRequestUsers endUpdates];
            if (isRequestViewOpen)
            {
                if (arrayRequests.count>0)
                {
                    self.labelRequestUser.text = [NSString stringWithFormat:@"%lu People have requested to follow you",(unsigned long)arrayRequests.count];
                    [self openRequestViewWithAnimation];
                }
                else
                {
                    isRequestViewOpen = NO;
                    [self closeRequestViewWithAnimation];
                    if (weakSelf.containerViewHeightConstraint.constant==0)
                    {
                        weakSelf.containerViewHeightConstraint.constant = [UIScreen mainScreen].bounds.size.width;
                        [UIView animateWithDuration:0.1 delay:0.0 usingSpringWithDamping:0.9 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                            [self.view layoutIfNeeded];
                        } completion:nil];
                        [arrayRequests removeAllObjects];
                        [weakSelf.tableRequestUsers reloadData];
                    }
                }
            }
        }
        else
        {
//            FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:0]];
//            
//            tableViewCell.btnCancelRequest.hidden = NO;
//            tableViewCell.btnFollowUnfollow.hidden = NO;
//            tableViewCell.loader.hidden = YES;
//            [tableViewCell.loader stopAnimating];
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          NSInteger newIndex = [arrayRequests indexOfObject:dict];
                                          [CommonFunction removeActivityIndicator];
                                          
                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:0]];
                                          
                                          tableViewCell.btnCancelRequest.hidden = NO;
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [weakSelf.tableRequestUsers reloadData];
                                      }];
}
- (void)onRequestCancel:(UIButton *)button
{
    [CommonFunction showActivityIndicatorWithText:@""];
    
    NSInteger index = button.tag;
    NSDictionary *dict = arrayRequests[index];
    NSString *userId = arrayRequests[index][@"user_id"];
    
    FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[self.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    tableViewCell.btnCancelRequest.hidden = YES;
    tableViewCell.btnFollowUnfollow.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    NSString *url = [NSString stringWithFormat:@"users/acceptFollowRequest/%@",userId];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSInteger newIndex = [arrayRequests indexOfObject:dict];
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:0]];
        
        tableViewCell.btnCancelRequest.hidden = NO;
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        if(responseDict==Nil){
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
//            FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:0]];
//            
//            tableViewCell.btnCancelRequest.hidden = NO;
//            tableViewCell.btnFollowUnfollow.hidden = NO;
//            tableViewCell.loader.hidden = YES;
//            [tableViewCell.loader stopAnimating];
        }
        else if([operation.response statusCode]==200)
        {
            [arrayRequests removeObject:dict];
            [weakSelf.tableRequestUsers beginUpdates];
            [weakSelf.tableRequestUsers deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:newIndex inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
            [weakSelf.tableRequestUsers endUpdates];
            
            if (isRequestViewOpen)
            {
                if (arrayRequests.count>0)
                {
                    self.labelRequestUser.text = [NSString stringWithFormat:@"%lu People have requested to follow you",(unsigned long)arrayRequests.count];
                    [self openRequestViewWithAnimation];
                }
                else
                {
                    isRequestViewOpen = NO;
                    [self closeRequestViewWithAnimation];
                    if (weakSelf.containerViewHeightConstraint.constant==0)
                    {
                        weakSelf.containerViewHeightConstraint.constant = [UIScreen mainScreen].bounds.size.width;
                        [UIView animateWithDuration:0.1 delay:0.0 usingSpringWithDamping:0.9 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                            [self.view layoutIfNeeded];
                        } completion:nil];
                        [arrayRequests removeAllObjects];
                        [weakSelf.tableRequestUsers reloadData];
                    }
                }
            }
        }
        else
        {
//            FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:0]];
//            
//            tableViewCell.btnCancelRequest.hidden = NO;
//            tableViewCell.btnFollowUnfollow.hidden = NO;
//            tableViewCell.loader.hidden = YES;
//            [tableViewCell.loader stopAnimating];
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          NSInteger newIndex = [arrayRequests indexOfObject:dict];
                                          [CommonFunction removeActivityIndicator];
                                          
                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableRequestUsers cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:0]];
                                          
                                          tableViewCell.btnCancelRequest.hidden = NO;
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [weakSelf.tableRequestUsers reloadData];
                                      }];
}


#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([self.tableActivities contentOffset].y + self.tableActivities.frame.size.height) >= [self.tableActivities contentSize].height)
    {
        if (stopRequest)
        {
            return;
        }
        NSLog(@"scrolled to bottom");
        if (!isPageRefreshing) {
            
            if(arrayActivities.count != requestLimit)
            {
                return;
            }
            else
            {
                currentPage++;
                [self animateFooterView];
                [self.tableActivities setTableFooterView:self.vwFooterVw];
            }
            
            isPageRefreshing = YES;
            [self getActivityList];
        }
    }
}

#pragma mark - FooterView Animating
-(void)animateFooterView
{
    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionRepeat|UIViewAnimationOptionCurveLinear
                     animations:^{
                         [self.imgFooterCircle setTransform:CGAffineTransformRotate([self.imgFooterCircle transform], M_PI-0.00001f)];
                     } completion:nil];
    
}


#pragma mark -
#pragma mark Web Services
- (void)getActivityList
{
    [CommonFunction showActivityIndicatorWithText:@""];
    
    NSString *url = [NSString stringWithFormat:@"users/activities/%ld",(long)currentPage];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        
        [self.tableActivities setTableFooterView:nil];
        
        if (refreshPullDown.isRefreshing)
        {
            AudioServicesPlaySystemSound(1109);
        }
        [refreshPullDown endRefreshing];
        
        if(responseDict==Nil){
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
        }
        else if([operation.response statusCode]==200)
        {
            isPageRefreshing = NO;
            requestLimit = [responseDict[@"limit"] integerValue];
            
            if (responseDict[@"activities"] && [responseDict[@"activities"] isKindOfClass:[NSArray class]])
            {
                NSArray *array = responseDict[@"activities"];
                if (array.count>0)
                {
                    if (currentPage == 1)
                    {
                        [arrayActivities removeAllObjects];
                    }
                    [arrayActivities addObjectsFromArray:[array mutableCopy]];
                    
                    for (NSInteger i=0; i<arrayActivities.count; i++)
                    {
                        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrayActivities[i][@"user_id"]])
                        {
                            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrayActivities[i][@"user_id"]];
                            BOOL requestAcess = [arrayActivities[i][@"request_status"] boolValue];
                            [dataManager.followersDataArray[index] setValue:[NSNumber numberWithBool:requestAcess] forKey:@"request_status"];
                            
                            BOOL isFolowing = [arrayActivities[i][@"is_following"] boolValue];
                            if (!isFolowing)
                            {
                                [dataManager.followersDataArray removeObjectAtIndex:index];
                            }
                        }
                        NSInteger someIndex = [[arrayActivities valueForKey:@"username"]indexOfObject:arrayActivities[i][@"username"]];
                        [arrayActivities[someIndex] setValue:@YES forKey:@"showDetail"];
                    }
                    [weakSelf.tableActivities reloadData];
                    stopRequest = NO;
                }
                else
                {
                    stopRequest = YES;
                }
            }
            if (responseDict[@"follow_requests"] && [responseDict[@"follow_requests"] isKindOfClass:[NSArray class]])
            {
                NSArray *array = responseDict[@"follow_requests"];
                if (array.count>0)
                {
                    self.labelRequestUser.text = [NSString stringWithFormat:@"%lu People have requested to follow you",(unsigned long)array.count];
                    if (weakSelf.containerViewHeightConstraint.constant==[UIScreen mainScreen].bounds.size.width)
                    {
                        weakSelf.containerViewHeightConstraint.constant = 0;
                        [UIView animateWithDuration:0.1 delay:0.0 usingSpringWithDamping:0.9 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                            [self.view layoutIfNeeded];
                        } completion:nil];
                    }
                    [arrayRequests removeAllObjects];
                    [arrayRequests addObjectsFromArray:[array mutableCopy]];
                    [weakSelf.tableRequestUsers reloadData];
                    if (isRequestViewOpen)
                    {
                        [self openRequestViewWithAnimation];
                    }
                }
                else
                {
                    if (weakSelf.containerViewHeightConstraint.constant==0)
                    {
                        weakSelf.containerViewHeightConstraint.constant = [UIScreen mainScreen].bounds.size.width;
                        [UIView animateWithDuration:0.1 delay:0.0 usingSpringWithDamping:0.9 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
                            [self.view layoutIfNeeded];
                        } completion:nil];
                        [arrayRequests removeAllObjects];
                        [weakSelf.tableRequestUsers reloadData];
                    }
                }
            }
            if (arrayActivities.count==0)
            {
                _labelErrorMessage.hidden = NO;
            }
            else
            {
                _labelErrorMessage.hidden = YES;
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [refreshPullDown endRefreshing];
                                          [self.tableActivities setTableFooterView:nil];
                                          [CommonFunction removeActivityIndicator];
                                          if (currentPage>1)
                                          {
                                              currentPage--;
                                          }
                                          isPageRefreshing = NO;
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}


-(void)postFollowersListAPIAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    [CommonFunction showActivityIndicatorWithText:@""];
 
    NSString *url = [NSString stringWithFormat:@"users/following"];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@[dataDict],@"following", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        ActivityCell *tableViewCell = (ActivityCell *)[self.tableActivities cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.activityFollowUnFollowBtn.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrayActivities[index] setValue:@1 forKey:@"is_following"];
            if ([arrayActivities[index][@"account_type"] isEqualToString:@"private"])
            {
                [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
                [arrayActivities[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            }
            else
            {
                [newDict setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
                [arrayActivities[index] setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
            }
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
                [dataManager.followersDataArray addObject:newDict];
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [self.tableActivities reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];
                                          
                                          ActivityCell *tableViewCell = (ActivityCell *)[self.tableActivities cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.activityFollowUnFollowBtn.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [self.tableActivities reloadData];
                                      }];
}


- (void)unFollowUserApiAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following/%@",dataDict[@"user_id"]];
    //http://192.168.0.22:8353/v1//users/following/{userId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserId,@"userId", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        ActivityCell *tableViewCell = (ActivityCell *)[self.tableActivities cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.activityFollowUnFollowBtn.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [CommonFunction removeActivityIndicatorWithWait];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrayActivities[index] setValue:@0 forKey:@"is_following"];
            
            [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            [arrayActivities[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [self.tableActivities reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];

                                          ActivityCell *tableViewCell = (ActivityCell *)[self.tableActivities cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          
                                          tableViewCell.activityFollowUnFollowBtn.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [self.tableActivities reloadData];
                                      }];
}



- (void)customTableViewCell:(FollowUnfollowCustomeCell *)cell didTapOnURL:(NSString *)urlString {
    
    WebViewVC *wvc=[[WebViewVC alloc]init];
    if (![urlString containsString:@"http"])
    {
        urlString = [NSString stringWithFormat:@"http://%@",urlString];
    }
    
    wvc.strWbUrl=urlString;
    [self.navigationController pushViewController:wvc animated:YES];
}


@end
