//
//  ForgotPasswordVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 17/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "ForgotPasswordVC.h"

@interface ForgotPasswordVC ()<UIAlertViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    __weak IBOutlet UIView *vwBcgroundVdo;
    __weak IBOutlet UITextField *txtEmailAddress;
        CMTime vdoCurrentTime;
}
@property(strong,nonatomic)AVPlayer *avPlayer;
@property(strong,nonatomic)AVPlayerItem *avPlayerItem;
@property(strong,nonatomic)AVAsset *avAsset;
@property(strong,nonatomic)AVPlayerLayer *avPlayerLayer;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@end

@implementation ForgotPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [txtEmailAddress setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    wbServiceCount=0;
    
    [self playVideo];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayer currentItem]];
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapGesture:)];
    tapGesture.delegate=self;
    tapGesture.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tapGesture];
}
-(IBAction)singleTapGesture:(id)sender
{
    [txtEmailAddress resignFirstResponder];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Play Video In Av Player Method

-(void)playVideo
{
    NSString *filepath1 = [[NSBundle mainBundle] pathForResource:@"newlogin" ofType:@"mov"];
    NSURL *fileURL1 = [NSURL fileURLWithPath:filepath1];
    
    self.avPlayer = [AVPlayer playerWithURL:fileURL1]; //
    
    self.avPlayerLayer = [AVPlayerLayer layer];
    
    [self.avPlayerLayer setPlayer:self.avPlayer];
    [self.avPlayerLayer setFrame:[UIScreen mainScreen].bounds];
    //[layer setBackgroundColor:[UIColor redColor].CGColor];
    [self.avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [vwBcgroundVdo.layer addSublayer:self.avPlayerLayer];
    [self.avPlayer play];
    
    self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    
}
#pragma mark - AvPlayer Background Forground Even Method

-(void) appEnteredForeground {
    // Application Goes In Forground and play video
    __weak typeof(self) weakSelf = self;
    [self.avPlayer seekToTime:vdoCurrentTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
        [weakSelf.avPlayer play];
    }];
}

-(void) appEnteredBackground {
    // Application Goes In Background
    [self.avPlayer pause];
    AVPlayerItem *currentItem = self.avPlayer.currentItem;
    
    vdoCurrentTime = currentItem.currentTime;
}
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    // video did reach end and repeate video
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}





- (IBAction)backBtnClicked:(id)sender {
    [self.view endEditing:YES];
    txtEmailAddress.text=@"";
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)nextBtnClicked:(id)sender {
    [self.view endEditing:YES];
    
    if ([CommonFunction isValueNotEmpty:txtEmailAddress.text]) {
        if ([CommonFunction IsValidEmail:txtEmailAddress.text]) {
            if ([CommonFunction reachabiltyCheck]) {
                [CommonFunction showActivityIndicatorWithText:@""];
                _nextButton.enabled = NO;
                [self forgotPasswordAPI];
            }
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"" message:@"Please enter valid email address." onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
    }
    else
    {
        [CommonFunction showAlertWithTitle:@"" message:@"Please enter email address." onViewController:self useAsDelegate:NO dismissBlock:nil];
    }
    
}

#pragma mark - UIAlertView Delegate Methd
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==200) {
        txtEmailAddress.text=@"";
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - UITextField Delegate method
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    return [txtEmailAddress resignFirstResponder];
    
}

#pragma mark - WebService API
-(void)forgotPasswordAPI
{
    NSString *url = [NSString stringWithFormat:@"users/forgotPassword/%@",[CommonFunction trimSpaceInString:txtEmailAddress.text]];
    //http://192.168.0.22:8353/v1/users
    
//    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
//    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtUsername.text],@"username",[CommonFunction trimSpaceInString:txtEmail.text],@"email",[CommonFunction trimSpaceInString:txtPassword.text],@"password",@"ios",@"device_type",pushDeviceToken,@"device_token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        _nextButton.enabled = YES;
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction showAlertWithTitle:@"" message:@"Server Error" onViewController:self useAsDelegate:NO dismissBlock:nil];
//            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
            
        {
            [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"msg"] onViewController:weakSelf useAsDelegate:YES dismissBlock:^{
                txtEmailAddress.text=@"";
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }];
        }
        else
        {
            [CommonFunction showAlertWithTitle:@"" message:[responseDict objectForKey:@"msg"] onViewController:self useAsDelegate:NO dismissBlock:nil];
//            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          _nextButton.enabled = YES;
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction showAlertWithTitle:@"" message:[operation.responseObject objectForKey:@"response"] onViewController:self useAsDelegate:NO dismissBlock:nil];
//                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [weakSelf forgotPasswordAPI];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction showAlertWithTitle:@"" message:@"Network Error..." onViewController:self useAsDelegate:NO dismissBlock:nil];
//                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
