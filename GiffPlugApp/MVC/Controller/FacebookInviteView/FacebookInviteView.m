//
//  FacebookInviteView.m
//  GiffPlugApp
//
//  Created by Santosh on 02/09/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "FacebookInviteView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "FollowUnfollowCustomeCell.h"
#import "OtherUserProfileVC.h"
#import "AppDataManager.h"


@interface FacebookInviteView ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *arrayFacebookFriends;
    AppDataManager *dataManager;
}
- (IBAction)onBackAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *labelNoRecord;
@property (weak, nonatomic) IBOutlet UITableView *tableFacebookFriends;
@end

@implementation FacebookInviteView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    dataManager = [AppDataManager sharedInstance];
    
    arrayFacebookFriends = [[NSMutableArray alloc]init];
    [self loginWithFacebook];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Login Facebook
- (void)loginWithFacebook
{
    __weak typeof(self) weakSelf = self;
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: @[@"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             weakSelf.view.userInteractionEnabled = YES;
             if (error) {
//                 [_tableFacebookFriends setHidden:YES];
                 [_labelNoRecord setHidden:NO];
                 NSLog(@"Process error");
             } else if (result.isCancelled) {
//                 [_tableFacebookFriends setHidden:YES];
                 [_labelNoRecord setHidden:NO];
                 NSLog(@"Cancelled");
             } else {
                 NSLog(@"Logged in");
                 [weakSelf checkFacebookGrantedPermissionsInResult:result];
             }
         });
     }];
}

#pragma mark - Check Facebook And Get Friends
- (void)checkFacebookGrantedPermissionsInResult:(FBSDKLoginManagerLoginResult *)result
{
    __weak typeof(self) weakSelf = self;
    
    FBSDKLoginManagerLoginResult * tempResult = result;
    BOOL isFriendsPermitted;
    isFriendsPermitted = YES;
    NSSet *grantedPermissions = tempResult.grantedPermissions;
    // Email permission not granted to user.
    if (![grantedPermissions containsObject:@"user_friends"])
    {
        isFriendsPermitted = NO;
    }
    if (isFriendsPermitted)
    {
        //success move forward...
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        // Get user details using graph api.
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/friends" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) { // Success
                 NSLog(@"fetched user:%@", result);
                 
                 // Login the user
                 [weakSelf parseFacebookResultAndPerformLogin:result];
             }
             else
             {
//                 [_tableFacebookFriends setHidden:YES];
                 [_labelNoRecord setHidden:NO];
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             }
         }];
    }
    else
    {
//        [_tableFacebookFriends setHidden:YES];
        [_labelNoRecord setHidden:NO];
    }
}

#pragma mark - Parse Facebook Friends
- (void)parseFacebookResultAndPerformLogin:(NSDictionary *)result
{
    if (result && result[@"data"] && [result[@"data"] isKindOfClass:[NSArray class]])
    {
        NSArray *array = result[@"data"];
        
        for (NSInteger i=0; i<array.count; i++)
        {
            NSString *fbID = array[i][@"id"];
            [arrayFacebookFriends addObject:fbID];
        }
    }
    if (arrayFacebookFriends.count>0)
    {
        [self onSubmitFacebookFriendsToServer];
    }
}

#pragma mark - Submit Friends Api
- (void)onSubmitFacebookFriendsToServer
{
    NSString *url = [NSString stringWithFormat:@"users/findFbusers"];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    [CommonFunction showActivityIndicatorWithText:@""];
    
    NSDictionary *params = @{
                             @"fbids" : arrayFacebookFriends
                             };
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:[params mutableCopy] withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
//            [_tableFacebookFriends setHidden:YES];
            [_labelNoRecord setHidden:NO];
        }
        else if([operation.response statusCode]==200)
        {
            [arrayFacebookFriends removeAllObjects];
            if (responseDict && responseDict[@"users"] && [responseDict[@"users"] isKindOfClass:[NSArray class]])
            {
                NSArray *array = responseDict[@"users"];
                if (array.count>0) {
                    [arrayFacebookFriends addObjectsFromArray:responseDict[@"users"]];
                }
                else
                {
//                    [_tableFacebookFriends setHidden:YES];
                    [_labelNoRecord setHidden:NO];
                }
            }
            [_tableFacebookFriends reloadData];
        }
        else
        {
//            [_tableFacebookFriends setHidden:YES];
            [_labelNoRecord setHidden:NO];
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
//                                          [_tableFacebookFriends setHidden:YES];
                                          [_labelNoRecord setHidden:NO];
                                          
                                          [CommonFunction removeActivityIndicator];

                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

#pragma mark - UITableView Datasource And Delegate Method
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayFacebookFriends.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"FollowerFollowingCustomeCell";
    
    FollowUnfollowCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell)
    {
        cell = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([FollowUnfollowCustomeCell class]) owner:self options:nil][1];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(FollowUnfollowCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (UIGestureRecognizer *gesture in cell.imageViewProfile.gestureRecognizers)
    {
        [cell.imageViewProfile removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tappGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(profileBtnCLicked:)];
    tappGesture.numberOfTapsRequired=1;
    [cell.imageViewProfile addGestureRecognizer:tappGesture];
    cell.imageViewProfile.tag = indexPath.row;
    
    
    cell.btnFollowUnfollow.tag=indexPath.row;
    [cell.btnFollowUnfollow addTarget:self action:@selector(plusMinusBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //    cell.imgProfileImg.tag=indexPath.row;
    //    [cell.imgProfileImg addTarget:self action:@selector(profileBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (arrayFacebookFriends.count>0) {
        NSString *strAvtarimg=[NSString stringWithFormat:@"%@",[[arrayFacebookFriends objectAtIndex:indexPath.row]valueForKey:@"avatar_image"]];
        
        if (strAvtarimg.length>1) {
            
            [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error)
                {
                    cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                }
                else
                {
                    if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleAspectFill;
                    }
                    else
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                    }
                }
            }];
            //            [cell.imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //
            //            }];
        }
        else
        {
            [cell.imageViewProfile setImage:[UIImage imageNamed:@"default.jpg"]];
            //            [cell.imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
        }
        
        if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrayFacebookFriends[indexPath.row][@"user_id"]])
        {
            NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrayFacebookFriends[indexPath.row][@"user_id"] ];
            BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
            if (requestAcess) {
                [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                cell.btnFollowUnfollow.hidden = NO;
                cell.labelRequested.hidden = YES;
            }
            else
            {
                cell.btnFollowUnfollow.hidden = YES;
                cell.labelRequested.hidden = NO;
            }
        }
        else
        {
            BOOL isFollowing = [arrayFacebookFriends [indexPath.row][@"is_following"]boolValue];
            
            if (isFollowing)
            {
                BOOL isRequestAccepted = [arrayFacebookFriends[indexPath.row][@"request_status"] boolValue];
                if (isRequestAccepted) {
                    [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                    cell.btnFollowUnfollow.hidden = NO;
                    cell.labelRequested.hidden = YES;
                }
                else
                {
                    cell.btnFollowUnfollow.hidden = YES;
                    cell.labelRequested.hidden = NO;
                }
            }
            else
            {
                cell.btnFollowUnfollow.hidden = NO;
                cell.labelRequested.hidden = YES;
                [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
            }
        }
        
        cell.lblUsername.text=[[arrayFacebookFriends objectAtIndex:indexPath.row]valueForKey:@"username"];
        
        NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
        if ([cell.lblUsername.text isEqualToString:loggedInUser])
        {
            cell.imgProfileImg.userInteractionEnabled = NO;
            cell.btnFollowUnfollow.hidden = YES;
        }
    }
}

#pragma mark - On Other User Action
- (void)profileBtnCLicked:(UITapGestureRecognizer *)sender {
    if (arrayFacebookFriends.count>0) {
        
        NSString *loggedInUser = [NSUSERDEFAULTS objectForKey:kUSERID];
        if ([loggedInUser isEqualToString:arrayFacebookFriends[sender.view.tag][@"username"]])
        {
            
        }
        else
        {
            OtherUserProfileVC *ouVC=[[OtherUserProfileVC alloc]init];
            ouVC.hidesBottomBarWhenPushed=YES;
            ouVC.strUserName=arrayFacebookFriends[sender.view.tag][@"username"];
            [self.navigationController pushViewController:ouVC animated:YES];
        }
    }
}

#pragma mark - On Follow/Unfollow Action
-(void)plusMinusBtnCLicked:(UIButton *)sender
{
    FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)sender.superview.superview;
    
    tableViewCell.btnFollowUnfollow.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[arrayFacebookFriends objectAtIndex:sender.tag]valueForKey:@"user_id"],@"user_id",[[arrayFacebookFriends objectAtIndex:sender.tag]valueForKey:@"account_type"],@"account_type", nil];
    
    if ([[arrayFacebookFriends[sender.tag][@"is_following"]stringValue] isEqualToString:@"0"]) {
        [self postFollowersListAPIAtIndex:sender.tag WithObject:dataDict];
    }
    else
    {
        [self unFollowUserApiAtIndex:sender.tag WithObject:dataDict];
    }
}

#pragma mark - Follow Api
-(void)postFollowersListAPIAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following"];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@[dataDict],@"following", nil];
    
    __weak typeof(self) weakSelf = self;
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[self.tableFacebookFriends cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrayFacebookFriends[index] setValue:@1 forKey:@"is_following"];
            if ([arrayFacebookFriends[index][@"account_type"] isEqualToString:@"private"])
            {
                [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
                [arrayFacebookFriends[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            }
            else
            {
                [newDict setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
                [arrayFacebookFriends[index] setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
            }
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
                [dataManager.followersDataArray addObject:newDict];
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [weakSelf.tableFacebookFriends reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[self.tableFacebookFriends cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [weakSelf.tableFacebookFriends reloadData];
                                      }];
}

#pragma mark - UnFollow Api
- (void)unFollowUserApiAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    NSString *url = [NSString stringWithFormat:@"users/following/%@",dataDict[@"user_id"]];
    //http://192.168.0.22:8353/v1//users/following/{userId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserId,@"userId", nil];
    
    __weak typeof(self) weakSelf = self;
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableFacebookFriends cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        [CommonFunction removeActivityIndicatorWithWait];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrayFacebookFriends[index] setValue:@0 forKey:@"is_following"];
            
            [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            [arrayFacebookFriends[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [weakSelf.tableFacebookFriends reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          [CommonFunction removeActivityIndicator];
                                          
                                          FollowUnfollowCustomeCell *tableViewCell = (FollowUnfollowCustomeCell *)[weakSelf.tableFacebookFriends cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [weakSelf.tableFacebookFriends reloadData];
                                      }];
}

#pragma mark - On Back Action
- (IBAction)onBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
