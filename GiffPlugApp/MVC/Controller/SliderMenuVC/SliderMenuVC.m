//
//  SliderMenuVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 16/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "SliderMenuVC.h"
#import "HomeFeedVC.h"
#import "OtherUserProfileVC.h"
#import "InfoCustomeCell.h"
#import "LikeVC.h"
#import "CommentVC.h"
#import "ReplugVC.h"
#import "AppDataManager.h"
@interface SliderMenuVC ()<UIGestureRecognizerDelegate>
{
    
    IBOutlet UIImageView *imgReplugIcon;
    IBOutlet UIImageView *imgCommentIcon;
    IBOutlet UIImageView *imgLikeIcon;
    IBOutlet UILabel *lblLikeCount;
    IBOutlet UILabel *lblCommentCount;
    IBOutlet UILabel *lblReplugCount;
    IBOutlet UILabel *lblPostedTime;
    IBOutlet UILabel *lblErrorMsg;
    NSMutableArray *arrFollwersData;
    NSMutableArray *expandedIndexPaths;
    NSMutableArray *arrSelectedIndexPath;
    IBOutlet UITableView *tblVw;
    IBOutlet UIButton *btnVwAll;

    BOOL ismyComment;
    BOOL ismyReplug;
    BOOL ismyLike;
    
    int intButtonTag;
    HomeFeedVC *hfvc;
    __weak IBOutlet UILabel *gifDate;
    
    AppDataManager *dataManager;
}
@property (weak, nonatomic) IBOutlet UILabel *labelGifCount;
@end

@implementation SliderMenuVC
@synthesize strPostID;

- (instancetype)init
{
    if (self=[super init]) {
        _dictPostsData = [[NSMutableDictionary alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    dataManager = [AppDataManager sharedInstance];
    [tblVw registerNib:[UINib nibWithNibName:NSStringFromClass([InfoCustomeCell class]) bundle:nil] forCellReuseIdentifier:@"InfoCustomeCell"];
    
    lblCommentCount.text=[NSString stringWithFormat:@"%d",[_dictPostsData[@"comments_count"] integerValue]];
    
    lblLikeCount.text=[NSString stringWithFormat:@"%d",[_dictPostsData[@"likes_count"]integerValue]];
    
    lblReplugCount.text=[NSString stringWithFormat:@"%d",[_dictPostsData[@"replugged_count"] integerValue]];

    gifDate.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[_dictPostsData[@"posted_date"] doubleValue]]];
    
    self.labelGifCount.text = [CommonFunction abbreviateNumber:self.gifCount];
    
    NSLog(@"%@",_dictPostsData);
    UISwipeGestureRecognizer *swipGesture=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeGestureClicked:)];
    swipGesture.direction=UISwipeGestureRecognizerDirectionRight;
    swipGesture.delegate=self;
    [self.view addGestureRecognizer:swipGesture];
    arrFollwersData=[[NSMutableArray alloc]init];
    
    NSLog(@"%@",_dictPostsData);
    
    if ([[_dictPostsData[@"is_like"] stringValue]isEqualToString:@"1"]) {
        [imgLikeIcon setImage:[UIImage imageNamed:@"SM_likeIconActive"]];
       // [lblLikeCount setTextColor:[UIColor colorWithRed:255.0f/255.0f green:32.0f/255.0f blue:86.0f/255.0f alpha:1]];

    }
    else
    {
        [imgLikeIcon setImage:[UIImage imageNamed:@"SM_likeFillIcon"]];
        //[lblLikeCount setTextColor:[UIColor colorWithRed:142.0f/255.0f green:142.0f/255.0f blue:142.0f/255.0f alpha:1]];
    }
    
//    if ([[_dictPostsData[@"is_comment"] stringValue]isEqualToString:@"1"]) {
//        [imgCommentIcon setImage:[UIImage imageNamed:@"SM_commentIconActive"]];
//        [lblCommentCount setTextColor:[UIColor whiteColor]];
//        
//    }
//    else
//    {
//        [imgCommentIcon setImage:[UIImage imageNamed:@"SM_commentIcon"]];
//        [lblCommentCount setTextColor:[UIColor colorWithRed:142.0f/255.0f green:142.0f/255.0f blue:142.0f/255.0f alpha:1]];
//    }
//    
//    if ([[_dictPostsData[@"is_replug"] stringValue]isEqualToString:@"1"]) {
//        [imgReplugIcon setImage:[UIImage imageNamed:@"SM_replugIconActive"]];
//        [lblReplugCount setTextColor:[UIColor whiteColor]];
//        
//    }
//    else
//    {
//        [imgReplugIcon setImage:[UIImage imageNamed:@"SM_replugIcon"]];
//        [lblReplugCount setTextColor:[UIColor colorWithRed:142.0f/255.0f green:142.0f/255.0f blue:142.0f/255.0f alpha:1]];
//    }
//    

    
   
    
    
    intButtonTag=0;
            [self getPostReplug:strPostID];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)swipeGestureClicked:(id)sender
{
    [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.view.frame = CGRectMake(320.0f, 0.0f, self.view.frame.size.width,  self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Button Methods
- (IBAction)btnBackClicked:(id)sender {
    [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.view.frame = CGRectMake(320.0f, 0.0f, self.view.frame.size.width,  self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
}

- (IBAction)btnLikeCommentReplugClicked:(UIButton *)sender {
    intButtonTag=(int)sender.tag;

        [self getPostReplug:strPostID];

}

- (IBAction)btnViewAllClicked:(id)sender {
    [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
//        self.view.alpha = 0;
        self.view.frame = CGRectMake(320.0f, 0.0f, self.view.frame.size.width,  self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
        UINavigationController *baseController = nil;
        if ([self.parentViewController isKindOfClass:[UITabBarController class]])
        {
            UITabBarController *tbBar=(UITabBarController *)self.parentViewController;
            NSArray *vwCntrollers=tbBar.viewControllers;
            baseController = vwCntrollers[tbBar.selectedIndex];
        }
        else
        {
            baseController = (UINavigationController*)self.parentViewController;
        }
        
        if (intButtonTag==0) {
            LikeVC *lvc=[[LikeVC alloc]init];
            lvc.strPostId=strPostID;
            lvc.strLikeCount=lblLikeCount.text;
            lvc.hidesBottomBarWhenPushed=YES;
            [baseController pushViewController:lvc animated:YES];
        }
        else if (intButtonTag==1)
        {
            CommentVC *lvc=[[CommentVC alloc]init];
            lvc.strPostId=strPostID;
            lvc.strLikeCount=lblCommentCount.text;
            lvc.hidesBottomBarWhenPushed=YES;
            [baseController pushViewController:lvc animated:YES];
        }
        else if (intButtonTag==2)
        {
            ReplugVC *lvc=[[ReplugVC alloc]init];
            lvc.strPostId=strPostID;
            lvc.strLikeCount=lblReplugCount.text;
            lvc.hidesBottomBarWhenPushed=YES;
            [baseController pushViewController:lvc animated:YES];
        }
        
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }];
    
}

-(IBAction)imgprofileBtnCLicked:(UITapGestureRecognizer *)sender
{

    if ([[arrFollwersData[sender.view.tag][@"is_my_profile"]stringValue]isEqualToString:@"0"] || [[arrFollwersData[sender.view.tag][@"is_my_comment"]stringValue]isEqualToString:@"0"]) {
        
        [UIView animateWithDuration:0.25 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveLinear animations:^{
            //        self.view.alpha = 0;
            self.view.frame = CGRectMake(320.0f, 0.0f, self.view.frame.size.width,  self.view.frame.size.height);
        } completion:^(BOOL finished) {
            
            if (![arrFollwersData[sender.view.tag][@"username"] isEqualToString:self.strUsername])
            {
                UINavigationController *baseController = nil;
                if ([self.parentViewController isKindOfClass:[UITabBarController class]])
                {
                    UITabBarController *tbBar=(UITabBarController *)self.parentViewController;
                    NSArray *vwCntrollers=tbBar.viewControllers;
                    baseController = vwCntrollers[tbBar.selectedIndex];
                }
                else
                {
                    baseController = (UINavigationController*)self.parentViewController;
                }
                
                OtherUserProfileVC *oupvc=[[OtherUserProfileVC alloc]initWithNibName:@"OtherUserProfileVC" bundle:nil];
                oupvc.hidesBottomBarWhenPushed=YES;
                oupvc.strUserName=arrFollwersData[sender.view.tag][@"username"];
                
                [baseController pushViewController:oupvc animated:YES];
            }
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
        }];
    }
}

-(IBAction)plusMinusBtnCLicked:(UIButton *)sender
{
    InfoCustomeCell *tableViewCell = (InfoCustomeCell *)sender.superview.superview;

    tableViewCell.btnFollowUnfollow.hidden = YES;
    tableViewCell.loader.hidden = NO;
    [tableViewCell.loader startAnimating];
    
    NSMutableDictionary *dataDict=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[arrFollwersData objectAtIndex:sender.tag]valueForKey:@"user_id"],@"user_id",[[arrFollwersData objectAtIndex:sender.tag]valueForKey:@"account_type"],@"account_type", nil];
    
        if ([[arrFollwersData[sender.tag][@"is_following"]stringValue] isEqualToString:@"0"]) {
            [self postFollowersListAPIAtIndex:sender.tag WithObject:dataDict];
        }
        else
        {
            [self unFollowUserApiAtIndex:sender.tag WithObject:dataDict];
        }
    }



-(void)postFollowersListAPIAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    [CommonFunction showActivityIndicatorWithText:@""];
    
    NSString *url = [NSString stringWithFormat:@"users/following"];
    //http://192.168.0.22:8353/v1/users/following
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@[dataDict],@"following", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        InfoCustomeCell *tableViewCell = (InfoCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrFollwersData[index] setValue:@1 forKey:@"is_following"];
            if ([arrFollwersData[index][@"account_type"] isEqualToString:@"private"])
            {
                [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
                [arrFollwersData[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            }
            else
            {
                [newDict setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
                [arrFollwersData[index] setValue:[NSNumber numberWithBool:YES] forKey:@"request_status"];
            }
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
                [dataManager.followersDataArray addObject:newDict];
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [tblVw reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];

                                          InfoCustomeCell *tableViewCell = (InfoCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [tblVw reloadData];
                                      }];
}


- (void)unFollowUserApiAtIndex:(NSInteger)index WithObject:(NSDictionary *)dataDict
{
    [CommonFunction showActivityIndicatorWithText:@""];
    
    NSString *url = [NSString stringWithFormat:@"users/following/%@",dataDict[@"user_id"]];
    //http://192.168.0.22:8353/v1//users/following/{userId}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    //    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserId,@"userId", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeDelete withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        InfoCustomeCell *tableViewCell = (InfoCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        
        tableViewCell.btnFollowUnfollow.hidden = NO;
        tableViewCell.loader.hidden = YES;
        [tableViewCell.loader stopAnimating];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithDictionary:dataDict];
            [arrFollwersData[index] setValue:@0 forKey:@"is_following"];
            
            [newDict setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            [arrFollwersData[index] setValue:[NSNumber numberWithBool:NO] forKey:@"request_status"];
            
            if (dataManager.followersDataArray) {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:newDict[@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:newDict[@"user_id"]];
                    [dataManager.followersDataArray removeObjectAtIndex:index];
                }
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
        [tblVw reloadData];
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];

                                          InfoCustomeCell *tableViewCell = (InfoCustomeCell *)[tblVw cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                          tableViewCell.btnFollowUnfollow.hidden = NO;
                                          tableViewCell.loader.hidden = YES;
                                          [tableViewCell.loader stopAnimating];
                                          
                                          if([operation.response statusCode]  == 400 ){
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          [tblVw reloadData];
                                      }];
}


#pragma mark - UITable View Delegate Methds

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return MIN(5, arrFollwersData.count);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f*SCREEN_YScale;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"InfoCustomeCell";
    
    InfoCustomeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(InfoCustomeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (UIGestureRecognizer *gesture in cell.imageViewProfile.gestureRecognizers)
    {
        [cell.imageViewProfile removeGestureRecognizer:gesture];
    }
    
    UITapGestureRecognizer *tappGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgprofileBtnCLicked:)];
    tappGesture.numberOfTapsRequired=1;
    [cell.imageViewProfile addGestureRecognizer:tappGesture];
    cell.imageViewProfile.tag = indexPath.row;
    
    cell.btnFollowUnfollow.tag=indexPath.row;
    [cell.btnFollowUnfollow addTarget:self action:@selector(plusMinusBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    
//    cell.imgProfileImg.tag=indexPath.row;
//    [cell.imgProfileImg addTarget:self action:@selector(imgprofileBtnCLicked:) forControlEvents:UIControlEventTouchUpInside];
    if (arrFollwersData.count>0) {
        if ([[arrFollwersData [indexPath.row][@"is_my_profile"]stringValue]isEqualToString:@"1"]||[[arrFollwersData [indexPath.row][@"is_my_comment"]stringValue]isEqualToString:@"1"]) {
            [cell.btnFollowUnfollow setHidden:YES];
            cell.labelRequested.hidden = YES;
        }
        else
        {
            if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollwersData[indexPath.row][@"user_id"]])
            {
                NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollwersData[indexPath.row][@"user_id"] ];
                BOOL requestAcess = [dataManager.followersDataArray[index][@"request_status"]boolValue];
                if (requestAcess) {
                    [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                    cell.btnFollowUnfollow.hidden = NO;
                    cell.labelRequested.hidden = YES;
                }
                else
                {
                    cell.btnFollowUnfollow.hidden = YES;
                    cell.labelRequested.hidden = NO;
                }
            }
            else
            {
                BOOL isFollowing = [arrFollwersData [indexPath.row][@"is_following"]boolValue];
                if (isFollowing)
                {
                    BOOL isRequestAccepted = [arrFollwersData[indexPath.row][@"request_status"] boolValue];
                    if (isRequestAccepted) {
                        [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_MinusIcon"] forState:UIControlStateNormal];
                        cell.btnFollowUnfollow.hidden = NO;
                        cell.labelRequested.hidden = YES;
                    }
                    else
                    {
                        cell.btnFollowUnfollow.hidden = YES;
                        cell.labelRequested.hidden = NO;
                    }
                }
                else
                {
                    cell.btnFollowUnfollow.hidden = NO;
                    cell.labelRequested.hidden = YES;
                    [cell.btnFollowUnfollow setImage:[UIImage imageNamed:@"FUFS_PlusIcon"] forState:UIControlStateNormal];
                }
            }
        }
        //lblErrormsg.text=@"";
        NSString *strAvtarimg=[NSString stringWithFormat:@"%@",[[arrFollwersData objectAtIndex:indexPath.row]valueForKey:@"avatar_image"]];
        
        if (strAvtarimg.length>1) {
            
            [cell.imageViewProfile sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error)
                {
                    cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                }
                else
                {
                    if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleAspectFill;
                    }
                    else
                    {
                        cell.imageViewProfile.contentMode=UIViewContentModeScaleToFill;
                    }
                }
            }];
            
            
//            [cell.imgProfileImg sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"default.jpg"]];
        }
        else
        {
            [cell.imageViewProfile setImage:[UIImage imageNamed:@"default.jpg"]];
//            [cell.imgProfileImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
        }
        
        cell.lblTime.text = [NSDate prettyTimestampSinceDate:[NSDate dateWithTimeIntervalSince1970:[arrFollwersData [indexPath.row][@"time"]doubleValue]]];
        
        
        //        [cell.lblTime setFont:[UIFont fontWithName:cell.lblTime.font.fontName size:(cell.lblTime.font.pointSize*SCREEN_XScale)]];
        
        cell.lblUsername.text=[[arrFollwersData objectAtIndex:indexPath.row]valueForKey:@"username"];
        cell.lblUserBio.text=[NSString stringWithFormat:@"%@",[[arrFollwersData objectAtIndex:indexPath.row]valueForKey:@"bio"]];
    }
    else
    {
        lblErrorMsg.text=@"No User Found.";
    }
    
    if (arrFollwersData.count>5)
    {
        btnVwAll.hidden = NO;
    }
    else
    {
        btnVwAll.hidden = YES;
    }
}

-(void)getPostReplug:(NSString*)strPost
{
     [lblLikeCount setTextColor:[UIColor colorWithRed:142.0f/255.0f green:142.0f/255.0f blue:142.0f/255.0f alpha:1]];
     [lblCommentCount setTextColor:[UIColor colorWithRed:142.0f/255.0f green:142.0f/255.0f blue:142.0f/255.0f alpha:1]];
      [imgCommentIcon setImage:[UIImage imageNamed:@"SM_commentIcon"]];
[imgReplugIcon setImage:[UIImage imageNamed:@"SM_replugIcon"]];

    [lblReplugCount setTextColor:[UIColor colorWithRed:142.0f/255.0f green:142.0f/255.0f blue:142.0f/255.0f alpha:1]];
    NSString *url;
    if (intButtonTag==0) {
        url = [NSString stringWithFormat:@"posts/like/%@/%@",strPost,@"1"];
        [lblLikeCount setTextColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
    }
    else if (intButtonTag==1)
    {
        url = [NSString stringWithFormat:@"posts/%@/comments/%@",strPost,@"1"];
        
        //[imgCommentIcon setImage:[UIImage imageNamed:@"SM_commentIconActive"]];
        
        [lblCommentCount setTextColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
    }
    else if (intButtonTag==2)
    {
    url= [NSString stringWithFormat:@"posts/%@/replugs/%@",strPost,@"1"];
        
            //[imgReplugIcon setImage:[UIImage imageNamed:@"SM_replugIconActive"]];
        [lblReplugCount setTextColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1]];
    }

    //http://192.168.0.22:8353/v1/users/posts/{postId}/replugs/{page}
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
                btnVwAll.hidden = YES;
        }
        else if([operation.response statusCode]==200)
        {
            NSMutableArray *tempArr=[responseDict objectForKey:@"users"];
            [arrFollwersData removeAllObjects];
            for (int i=0; i<tempArr.count; i++) {
                NSString *strUserName=[[tempArr objectAtIndex:i]objectForKey:@"username"];

                if (![[arrFollwersData valueForKey:@"username"] containsObject:strUserName]) {
                     [arrFollwersData addObject:[tempArr objectAtIndex:i]];
                }
            }
            [btnVwAll setHidden:NO];
            
            for (NSInteger i=0; i<arrFollwersData.count; i++)
            {
                if ([[dataManager.followersDataArray valueForKey:@"user_id"]containsObject:arrFollwersData[i][@"user_id"]])
                {
                    NSInteger index = [[dataManager.followersDataArray valueForKey:@"user_id"]indexOfObject:arrFollwersData[i][@"user_id"]];
                    BOOL requestAcess = [arrFollwersData[i][@"request_status"] boolValue];
                    [dataManager.followersDataArray[index] setValue:[NSNumber numberWithBool:requestAcess] forKey:@"request_status"];
                    
                    BOOL isFolowing = [arrFollwersData[i][@"is_following"] boolValue];
                    if (!isFolowing)
                    {
                        [dataManager.followersDataArray removeObjectAtIndex:index];
                    }
                }
            }
            
            [tblVw reloadData];
            if (!(arrFollwersData.count>0)) {
                [btnVwAll setHidden:YES];
                if (intButtonTag==0) {
                lblErrorMsg.text=@"No Likes Found.";
                }
                else if (intButtonTag==1)
                {
                lblErrorMsg.text=@"No Comments Found.";
                }
                else if (intButtonTag==2)
                {
                lblErrorMsg.text=@"No Replugs Found.";
                }
            }
            else
            {
                lblErrorMsg.text=@"";
            }
            
        }
        else
        {
            btnVwAll.hidden = YES;
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          btnVwAll.hidden = YES;
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [self getPostReplug:strPost];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


@end
