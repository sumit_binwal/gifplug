//
//  GuidedContentScreen.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 21/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "GuidedContentScreen.h"
#import "GuidedContentCollectionCell.h"
#import "FollowUnFollowScreen.h"
@interface GuidedContentScreen ()<UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>
{
    
    IBOutlet UIImageView *imgSelectedDot1;
    IBOutlet UIImageView *imgSelectedDot2;
    IBOutlet UIImageView *imgSelectedDot3;
    IBOutlet UIImageView *imgSelectedDot4;
    IBOutlet UIImageView *imgSelectedDot5;
    IBOutlet UICollectionView *catCollectionVw;
    NSMutableArray *arrSelectedIndexPath;
    NSMutableArray *arrCatData;
}
@end

@implementation GuidedContentScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [catCollectionVw registerNib:[UINib nibWithNibName:@"GuidedContentCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"GuidedContentCollectionCell"];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    arrSelectedIndexPath=[[NSMutableArray alloc]init];
    
    wbServiceCount=1;
    
    if ([CommonFunction reachabiltyCheck]) {
        [CommonFunction showActivityIndicatorWithText:@""];
        [self showCatAPI];
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
 
    CGFloat screenWidth = catCollectionVw.frame.size.width;
    
    float cellWidth = screenWidth / 3.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth-10, cellWidth-10);
    
    [self.view layoutIfNeeded];
    return size;

//    return CGSizeMake(92.0f, 92.0f);
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GuidedContentCollectionCell *cell=(GuidedContentCollectionCell *)[catCollectionVw dequeueReusableCellWithReuseIdentifier:@"GuidedContentCollectionCell" forIndexPath:indexPath];
    CGFloat screenWidth = catCollectionVw.frame.size.width;
    float cellWidth = screenWidth / 3.0; //Replace the divisor with the column count requirement. Make sure to have it in
    cellWidth=cellWidth-10;
    
    cell.vwBgVw.layer.borderWidth=1.0f;
    cell.vwBgVw.layer.borderColor=[UIColor whiteColor].CGColor;    
    cell.vwBgVw.layer.cornerRadius=cellWidth/2;
    cell.vwBgVw.clipsToBounds=YES;

    if ([arrSelectedIndexPath containsObject:[[arrCatData objectAtIndex:indexPath.row] valueForKey:@"id"]])
    {
         [cell.vwBgVw setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:33.0f/255.0f blue:87.0f/255.0f alpha:1]];
    }
    else
    {
         [cell.vwBgVw setBackgroundColor:[UIColor clearColor]];
    }
    
    cell.lblCateLbl.text=[[arrCatData objectAtIndex:indexPath.row] valueForKey:@"title"];
    [cell.imgVwCatImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrCatData objectAtIndex:indexPath.row]valueForKey:@"image"]]]];
    
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrCatData.count;
}

-(IBAction)cellBtnClicked:(UIButton *)sender
{
    GuidedContentCollectionCell *clickedCell = (GuidedContentCollectionCell *)[[sender superview] superview];
    //NSIndexPath *clickedButtonIndexPath = [catCollectionVw indexPathForCell:clickedCell];
    [clickedCell.vwBgVw setBackgroundColor:[UIColor redColor]];
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GuidedContentCollectionCell *clickedCell = (GuidedContentCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];

    if ([arrSelectedIndexPath containsObject:[[arrCatData objectAtIndex:indexPath.row] valueForKey:@"id"]])
    {
        //[arrSelectedIndexPath removeObject:indexPath];
        [arrSelectedIndexPath removeObject:[[arrCatData objectAtIndex:indexPath.row] valueForKey:@"id"]];
        [clickedCell.vwBgVw setBackgroundColor:[UIColor clearColor]];
        [self setSelectedDotImage:arrSelectedIndexPath.count];
    }
    else
    {
        if (arrSelectedIndexPath.count<5) {
        [clickedCell.vwBgVw setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:33.0f/255.0f blue:87.0f/255.0f alpha:1]];
        //[arrSelectedIndexPath addObject:indexPath];
        [arrSelectedIndexPath addObject:[[arrCatData objectAtIndex:indexPath.row] valueForKey:@"id"]];
        [self setSelectedDotImage:arrSelectedIndexPath.count];
        }
        else
        { 
            [CommonFunction alertTitle:@"" withMessage:@"You can select maximum 5 categories."];
        }
    }
    NSLog(@"%@",arrSelectedIndexPath);
}

-(void)setSelectedDotImage:(NSUInteger)value
{
        [imgSelectedDot1 setImage:[UIImage imageNamed:@"GCS_WhiteDot"]];
        [imgSelectedDot2 setImage:[UIImage imageNamed:@"GCS_WhiteDot"]];
        [imgSelectedDot3 setImage:[UIImage imageNamed:@"GCS_WhiteDot"]];
        [imgSelectedDot4 setImage:[UIImage imageNamed:@"GCS_WhiteDot"]];
        [imgSelectedDot5 setImage:[UIImage imageNamed:@"GCS_WhiteDot"]];
    switch (value) {
        case 1:
        {
        [imgSelectedDot1 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            break;
        }
        case 2:
        {
            [imgSelectedDot1 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot2 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            break;
        }
        case 3:
        {
            [imgSelectedDot1 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot2 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot3 setImage:[UIImage imageNamed:@"GCS_RedDot"]];

            break;
        }
        case 4:
        {
            [imgSelectedDot1 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot2 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot3 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot4 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            break;
        }
        case 5:
        {
            [imgSelectedDot1 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot2 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot3 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot4 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            [imgSelectedDot5 setImage:[UIImage imageNamed:@"GCS_RedDot"]];
            break;
        }

            
        default:
            break;
    }
    
}

- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBActionBtnClicked
- (IBAction)nextBtnClicked:(id)sender {
    
    if (arrSelectedIndexPath.count>0) {
        if ([CommonFunction reachabiltyCheck]) {
            [CommonFunction showActivityIndicatorWithText:@""];
            [self saveCategoryAPI];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
    else
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please select at least one category."];
    }
}


#pragma mark - WebService API
-(void)showCatAPI
{
    
    NSString *url = [NSString stringWithFormat:@"categories"];
    //http://192.168.0.22:8353/v1/categories
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
            
        {
            arrCatData=[responseDict objectForKey:@"categories"];
            [catCollectionVw reloadData];
//            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"token"] forKey:kUSERTOKEN];
//            [NSUSERDEFAULTS synchronize];
//            GuidedContentScreen *gcs=[[GuidedContentScreen alloc] initWithNibName:@"GuidedContentScreen" bundle:nil];
//            [self.navigationController pushViewController:gcs animated:YES];
            
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [weakSelf showCatAPI];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


#pragma mark - WebService API
-(void)saveCategoryAPI
{
    NSLog(@"%@",[NSUSERDEFAULTS valueForKey:kUSERTOKEN]);
    NSString *url = [NSString stringWithFormat:@"users/findByCategory"];
    //http://192.168.0.22:8353/v1/users/findByCategory
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:arrSelectedIndexPath,@"categories", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
            
        {
            FollowUnFollowScreen *fus=[[FollowUnFollowScreen alloc]initWithNibName:@"FollowUnFollowScreen" bundle:nil];
            [NSUSERDEFAULTS setObject:arrSelectedIndexPath forKey:kFOLLOWINGCATEGORY];
            [NSUSERDEFAULTS setObject:@"1" forKey:kISCATEGORYSUBMITTED];
            [NSUSERDEFAULTS synchronize];
            fus.arrFollowersData=[responseDict objectForKey:@"users"];
            [weakSelf.navigationController pushViewController:fus animated:YES];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [CommonFunction removeActivityIndicator];
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [CommonFunction showActivityIndicatorWithText:@""];
                                                  [weakSelf saveCategoryAPI];
                                              }
                                              else
                                              {
                                                  [CommonFunction removeActivityIndicator];
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

@end
