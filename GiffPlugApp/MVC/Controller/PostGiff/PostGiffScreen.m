//
//  PostGiffScreen.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 05/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "PostGiffScreen.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Social/Social.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+GIF.h"
#import "PhotoPickerController.h"
#import "UserSuggestCell.h"
#import "CEMovieMaker.h"
#import "PHAsset+Utility.h"
#import "UIImage+Resize.h"


@interface PostGiffScreen ()<NSLayoutManagerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    float textViewNormalheight;
    NSString *lastString;
    NSString *captionText;
    UITableView *tableViewSuggestion;
    NSInteger currentPage;
    NSInteger limit;
    BOOL isPageRefreshing;
    NSMutableArray *arraySuggestedUsers;
    UITapGestureRecognizer *tapGesture;
    BOOL isSuggesting;
    BOOL stopRequest;
    NSURL *savedGifURL;
    NSString *shareType;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *captionHeightContraint;
@property (weak, nonatomic) IBOutlet UITextView *captionTextView;

@property (weak, nonatomic) IBOutlet UILabel *captionPlaceholderLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBarHeightConstraint;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@end

@implementation PostGiffScreen

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];

    //UnHide status bar
    
//    self.watermarkName.text = [NSUSERDEFAULTS objectForKey:kUSERID];
//    self.watermarkView.frame = CGRectMake(10, 10, 5+26+4+self.watermarkName.intrinsicContentSize.width+5, 34);
//    self.watermarkView.layer.borderColor = [[UIColor whiteColor]colorWithAlphaComponent:0.2].CGColor;
//    self.watermarkView.layer.borderWidth = 2.5f;
    
//    [self.view addSubview:self.watermarkView];
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
    shareType = @"";
    // Do any additional setup after loading the view from its nib.
    self.captionTextView.layoutManager.delegate = self;
    self.captionHeightContraint.constant = self.captionHeightContraint.constant * (SCREEN_WIDTH/320);
    textViewNormalheight = self.captionHeightContraint.constant;
//    tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapGesture)];
//    tapGesture.cancelsTouchesInView = NO;
//    tapGesture.numberOfTapsRequired = 1;
//    [self.view addGestureRecognizer:tapGesture];
    
    _progressBarHeightConstraint.constant = 4.0f*(SCREEN_WIDTH/320);
    
    if (!self.isGifFromGallery)
    {
        if ([NSUSERDEFAULTS objectForKey:@"progressValue"]) {
            float progresValue = [[NSUSERDEFAULTS objectForKey:@"progressValue"] floatValue];
            
            _progressBar.progress = progresValue;
        }
        else
        {
            _progressBar.progress = 0;
        }
    }
    else
    {
        _progressBar.hidden=YES;
    }
    [self playGif];
    currentPage = 1;
//    limit = 0;
    isPageRefreshing = YES;
    
//    if (_isGifFromGallery) {
//        NSData *data = [NSUSERDEFAULTS objectForKey:@"selectedGif"];
//        NSDictionary *dict = [CommonFunction animatedGIFWithData:data];
//        if (!_giffImagePartsArray) {
//            _giffImagePartsArray = [NSMutableArray array];
//        }
//        [self.giffImagePartsArray removeAllObjects];
//        [self.giffImagePartsArray addObjectsFromArray:[dict[@"images"]mutableCopy]];
//        _animationDuration = [dict[@"duration"] floatValue];
//    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self registerForKeyboardNotifications];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self deRegisterForKeyboardNotifications];
    [super viewWillDisappear:animated];
}

- (void)dealloc
{
    self.giffImageView.image = nil;
}

#pragma mark - Memory Managment

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.view];
    if (CGRectContainsPoint(tableViewSuggestion.frame, location))
    {
        
    }
    else
    {
        [self onTapGesture];
    }
}

- (void)onTapGesture
{
    [self.captionTextView resignFirstResponder];
    [self removeSuggestionListForUsernames];
}

- (void)playGif
{
    if (self.isGifFromGallery)
    {
        NSData *data = [NSUSERDEFAULTS objectForKey:@"selectedGif"];
        [self.giffImageView setImage:[UIImage sd_animatedGIFWithData:data]];
    }
    else
    {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dict in self.giffImagePartsArray) {
            UIImage *image = dict[@"image"];
            [array addObject:image];
        }
        
        UIImage *animatedImage = [UIImage animatedImageWithImages:array duration:self.animationDuration];
        [self.giffImageView setImage:animatedImage];
    }
//    self.giffImageView.animationImages = array;
//    self.giffImageView.animationDuration = self.animationDuration;
//    self.giffImageView.animationRepeatCount = 0;
//    [self.giffImageView startAnimating];
}

#pragma mark - NSLayoutManager Delegate
- (CGFloat)layoutManager:(NSLayoutManager *)layoutManager lineSpacingAfterGlyphAtIndex:(NSUInteger)glyphIndex withProposedLineFragmentRect:(CGRect)rect
{
    return 5.0f;
}


#pragma mark - Post Giff On Server

- (IBAction)postBtnPressed:(id)sender {
    
    if (![CommonFunction reachabiltyCheck])
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        return;
    }
    self.postBtn.enabled = NO;
    NSURL *gifUrl = [NSURL URLWithString:@""];
    if (!self.isGifFromGallery)
    {
        gifUrl=[CommonFunction createGiffFromImageArray:self.giffImagePartsArray andDelayTime:self.animationDuration/self.giffImagePartsArray.count isProfileUpload:NO];
        savedGifURL = [gifUrl copy];
    }
    NSDictionary *params = @{@"caption":self.captionTextView.text, @"url":gifUrl, @"isFromGallery":[NSNumber numberWithBool:self.isGifFromGallery], @"shareType":shareType};
    
    [APPDELEGATE goToHomeFeed];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"uploadgif" object:params];
}

#pragma mark - Share Giff on various social Networks

- (IBAction)shareGiffActions:(id)sender {
    UIButton *btn = (UIButton *)sender;
    
    switch (btn.tag) {
        case 1: // FACEBOOK
        {
            shareType = @"FACEBOOK";
            [self postBtnPressed:nil];
        }
            break;
        case 2:
        {
            shareType = @"TWITTER";
            [self postBtnPressed:nil];
//            NSURL *gifUrl = nil;
//            if (!self.isGifFromGallery)
//            {
//                gifUrl =[CommonFunction createGiffFromImageArray:self.giffImagePartsArray andDelayTime:self.animationDuration/self.giffImagePartsArray.count isProfileUpload:NO];
//                savedGifURL = [gifUrl copy];
//            }
//            if (![CommonFunction reachabiltyCheck])
//            {
//                [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
//                return;
//            }
//            [CommonFunction showActivityIndicatorWithText:@"Posting..."];
//            NSData *gifData = nil;
//            if (self.isGifFromGallery)
//            {
//                gifData = [NSUSERDEFAULTS objectForKey:@"selectedGif"];
//            }
//            else
//            {
//                gifData = [NSData dataWithContentsOfURL:gifUrl];
//            }
            //            [self uploadGifToTwitterWithData:gifData];
        }
            break;
            
        case 3:// Instagram
        {
            
//            if (self.giffImagePartsArray.count==0)
//            {
//                if (_isGifFromGallery) {
//                    NSData *data = [NSUSERDEFAULTS objectForKey:@"selectedGif"];
//                    NSDictionary *dict = [CommonFunction animatedGIFWithData:data];
//                    [self.giffImagePartsArray removeAllObjects];
//                    [self.giffImagePartsArray addObjectsFromArray:[dict[@"images"]mutableCopy]];
//                    _animationDuration = [dict[@"duration"] floatValue];
//                }
//            }
            
            shareType = @"INSTAGRAM";
            [self postBtnPressed:nil];
//            NSDictionary *settings = [CEMovieMaker videoSettingsWithCodec:AVVideoCodecH264 withWidth:704 andHeight:1264];
//            CEMovieMaker *maker = [[CEMovieMaker alloc]initWithSettings:settings andDelayTime:_animationDuration];
//            NSMutableArray *array = [NSMutableArray array];
//            for (NSInteger i=0; i<self.giffImagePartsArray.count ; i++)
//            {
//                if ([self.giffImagePartsArray[i] isKindOfClass:[NSDictionary class]])
//                {
//                    NSDictionary *dict = self.giffImagePartsArray[i];
//                    
//                    UIImage *image = dict[@"image"];
//                    
//                    [array addObject:[image resizedImageToFitInSize:CGSizeMake(704, 1264) scaleIfSmaller:NO]];
//                }
//                else
//                {
//                    UIImage *image = self.giffImagePartsArray[i];
//                    
//                    [array addObject:[image resizedImageToFitInSize:CGSizeMake(704, 1264) scaleIfSmaller:NO]];
//                }
//            }
//
//            __weak typeof(self) weakSelf = self;
//            [maker createMovieFromImages:array withCompletion:^(NSURL *fileURL) {
//                if ([[[UIDevice currentDevice]systemVersion]floatValue]<9)
//                {
//                    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//                    [library writeVideoAtPathToSavedPhotosAlbum:fileURL completionBlock:^(NSURL *assetURL, NSError *error) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [NSUSERDEFAULTS setObject:fileURL.absoluteString forKey:@"instaSave"];
//                            [weakSelf postBtnPressed:nil];
//                        });
//                    }];
//                }
//                else
//                {
//                    [PHAsset saveVideoAtURL:fileURL location:nil completionBlock:^(PHAsset *asset, BOOL success) {
//                        [[PHImageManager defaultManager]requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                NSArray *arr=[info[@"PHImageFileSandboxExtensionTokenKey"] componentsSeparatedByString:@";/"];
//                                NSArray *arr1 = [[arr lastObject] componentsSeparatedByString:@"\""];
//                                NSString *videoURL = [NSString stringWithFormat:@"%@",[arr1 firstObject]];
//                                [NSUSERDEFAULTS setObject:videoURL forKey:@"instaSave"];
//                                [weakSelf postBtnPressed:nil];
//                            });
//                        }];
//                    }];
//                }
//            }];
        }
            break;
            
        case 4:
        {
            shareType = @"SAVE";
            if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied || [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted)
            {
                PhotoPickerController *photoPicker = [[PhotoPickerController alloc]initWithNibName:NSStringFromClass([PhotoPickerController class]) bundle:nil];
                [self.navigationController presentViewController:photoPicker animated:YES completion:nil];
            }
            else
            {
                
                NSURL *gifUrl = nil;
                if (!self.isGifFromGallery)
                {
                    gifUrl =[CommonFunction createGiffFromImageArray:self.giffImagePartsArray andDelayTime:self.animationDuration/self.giffImagePartsArray.count isProfileUpload:NO];
                    savedGifURL = [gifUrl copy];
                }
                
                [CommonFunction showActivityIndicatorWithText:@""];
                
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                    NSData *gifData = nil;
                    if (self.isGifFromGallery)
                    {
                        gifData = [NSUSERDEFAULTS objectForKey:@"selectedGif"];
                    }
                    else
                    {
                        gifData = [NSData dataWithContentsOfURL:gifUrl];
                    }
                    
                    NSMutableArray *gifArray = [NSMutableArray array];
                    NSDictionary *dict = [CommonFunction animatedGIFWithData:gifData];
                    [gifArray removeAllObjects];
                    [gifArray addObjectsFromArray:[dict[@"images"]mutableCopy]];
                    CGFloat duration = [dict[@"duration"] floatValue];
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    
                    NSURL *url=[CommonFunction createGiffFromImageArray:gifArray andDelayTime:duration/gifArray.count isProfileUpload:NO];
                    
                    NSData *newGifData = [NSData dataWithContentsOfURL:url];
                    
                    
                    
                    if ([[[UIDevice currentDevice]systemVersion]floatValue]<9)
                    {
                        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                        [library writeImageDataToSavedPhotosAlbum:newGifData metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (!error)
                                {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Your Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else
                                {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                    else
                    {
                        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                            [[PHAssetCreationRequest creationRequestForAsset]addResourceWithType:PHAssetResourceTypePhoto data:newGifData options:nil];
                        } completionHandler:^(BOOL success, NSError *error) {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [CommonFunction removeActivityIndicator];
                                if (success) {
                                    [CommonFunction showAlertWithTitle:@"Gif Saved" message:@"Your Gif has been saved to Gallery" onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                                else {
                                    [CommonFunction showAlertWithTitle:@"Error" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                                }
                            });
                        }];
                    }
                    
                });
                
                
//                });
            }
        }
            break;
            
        default:
            break;
    }
}


-(void)uploadGifToTwitterWithData:(NSData *)gifData {
    
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    [account requestAccessToAccountsWithType:accountType options:nil
                                  completion:^(BOOL granted, NSError *error)
     {
         if (granted == YES)
         {
             NSArray *arrayOfAccounts = [account
                                         accountsWithAccountType:accountType];
             
             if ([arrayOfAccounts count] > 0)
             {
                 ACAccount *twitterAccount =
                 [arrayOfAccounts lastObject];
                 
                 NSURL *requestURL = [NSURL URLWithString:@"https://upload.twitter.com/1.1/media/upload.json"];
                 
                 SLRequest *postRequest = [SLRequest
                                           requestForServiceType:SLServiceTypeTwitter
                                           requestMethod:SLRequestMethodPOST
                                           URL:requestURL parameters:nil];
                 
                 postRequest.account = twitterAccount;
                 
                 [postRequest addMultipartData:gifData
                                      withName:@"media"
                                          type:@"image/gif"
                                      filename:@"gifplug.gif"];
                 [postRequest
                  performRequestWithHandler:^(NSData *responseData,
                                              NSHTTPURLResponse *urlResponse, NSError *error)
                  {
                      if (error)
                      {
                          [CommonFunction removeActivityIndicator];
                          [CommonFunction showAlertWithTitle:@"Posting Failed" message:error.localizedDescription onViewController:self useAsDelegate:NO dismissBlock:nil];
                      }
                      else
                      {
                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
                          
                          NSString *mediaID = [json objectForKey:@"media_id_string"];
                          
                          if (mediaID!=nil)
                          {
                              self.captionTextView.text = [self.captionTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                              NSURL *requestURL2 = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                              NSDictionary *message2 = @{@"status": self.captionTextView.text,
                                                         @"media_ids": mediaID };
                              
                              SLRequest *postRequest2 = [SLRequest
                                                         requestForServiceType:SLServiceTypeTwitter
                                                         requestMethod:SLRequestMethodPOST
                                                         URL:requestURL2 parameters:message2];
                              postRequest2.account = twitterAccount;
                              
                              [postRequest2
                               performRequestWithHandler:^(NSData *responseData,
                                                           NSHTTPURLResponse *urlResponse, NSError *error)
                               {
                                   [CommonFunction removeActivityIndicator];
                                   // DONE!!!
                                   //                                       NSDictionary *resultJson = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
                               }];
                          }
                          else
                          {
                              [CommonFunction removeActivityIndicator];
                              [CommonFunction showAlertWithTitle:@"Posting Failed" message:@"Due to some reason Gif posting failed, try again later!" onViewController:self useAsDelegate:NO dismissBlock:nil];
                          }

                      }
                }];
             }
             else
             {
                 [CommonFunction removeActivityIndicator];
                 [CommonFunction showAlertWithTitle:@"No Twiiter Accounts" message:@"There is no twitter accounts linked to your device, you can link one by going to Settings -> Twitter." onViewController:self useAsDelegate:NO dismissBlock:nil];
             }
         }
         else
         {
             [CommonFunction removeActivityIndicator];
             [CommonFunction showAlertWithTitle:@"Access Denied" message:@"When asked you denied the acces to twitter accounts, please enable access manually by going to settings." onViewController:self useAsDelegate:NO dismissBlock:nil];
         }
     }];
}

#pragma mark - View Back Btns

- (IBAction)backBtnPressed:(id)sender {

    [self.giffImageView stopAnimating];
    self.giffImageView.animationImages=nil;
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark -
#pragma mark UITextView Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self removeSuggestionListForUsernames];
//    if (textView.text.length == 0)
//    {
        [self.view layoutIfNeeded];
        self.captionHeightContraint.constant = 60 + textViewNormalheight;
        [UIView animateWithDuration:.25 delay:0 options:7 animations:^{
            [self.view layoutIfNeeded];
            textView.scrollEnabled = NO;
            textView.scrollEnabled = YES;
        } completion:nil];
//    }
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [self removeSuggestionListForUsernames];
    
    textView.text = [textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CGFloat newHeight = textViewNormalheight;
    if (textView.text.length > 0)
    {
        newHeight = textView.contentSize.height;
    }
//    if (textView.text.length == 0)
//    {
        [self.view layoutIfNeeded];
        self.captionHeightContraint.constant = newHeight;
//        self.captionHeightContraint.constant -= 60;
        [UIView animateWithDuration:.25 delay:0 options:7 animations:^{
            [self.view layoutIfNeeded];
            textView.scrollEnabled = NO;
            textView.scrollEnabled = YES;
        } completion:nil];
//    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length > 0) {
        self.captionPlaceholderLabel.hidden=YES;
    }else{
        self.captionPlaceholderLabel.hidden=NO;
    }
    textView.scrollEnabled = NO;
    textView.scrollEnabled = YES;
    
    captionText = textView.text;
    
    NSArray *worsCount = [textView.text componentsSeparatedByString:@" "];
    if (worsCount && worsCount.count>0)
    {
        lastString = [worsCount lastObject];
        if ([lastString hasPrefix:@"@"] && lastString.length>1)
        {
            isSuggesting = YES;
            [self showSuggestionListForUsernames:[lastString substringFromIndex:1]];
        }
        else
        {
            isSuggesting = NO;
            [self removeSuggestionListForUsernames];
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    currentPage = 1;
    isPageRefreshing = YES;
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    NSString *searchString;
    if (text.length > 0) {
        searchString = [NSString stringWithFormat:@"%@%@",textView.text, text];
//        if ([text isEqualToString:@"@"])
//        {
//            [self showSuggestionListForUsernames]
//        }
    } else {
        if (textView.text.length>0)
        {
            searchString = [textView.text substringToIndex:[textView.text length] - 1];
        }
        else
        {
            searchString = @"";
        }
    }
    
    if (searchString.length>140)
    {
        return NO;
    }
    
    return YES;
}


#pragma mark -
#pragma mark UIKeyBoard Activity Handling
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWasShown:)
//                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification object:nil];
    
}

- (void)deRegisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGFloat heightRemained = self.view.frame.size.height - kbSize.height;
    
    CGFloat heightToShift = self.shareBtnContainer.frame.origin.y - heightRemained;
    
    
    NSTimeInterval timDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey]integerValue];

    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationDuration:timDuration];
    self.view.transform = CGAffineTransformMakeTranslation(0, -heightToShift);
    [UIView commitAnimations];
}

- (void)keyboardWillChangeFrame:(NSNotification *)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGFloat heightRemained = self.view.frame.size.height - kbSize.height;
    
    CGFloat heightToShift = self.shareBtnContainer.frame.origin.y - heightRemained;
    
    
    NSTimeInterval timDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey]integerValue];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationDuration:timDuration];
    self.view.transform = CGAffineTransformMakeTranslation(0, -heightToShift);
    [UIView commitAnimations];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    
    NSTimeInterval timDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    UIViewAnimationCurve curve = [[info objectForKey:UIKeyboardAnimationCurveUserInfoKey]integerValue];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationDuration:timDuration];
    self.view.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}


-(void)uploadImageBySessionUserMethod:(NSString *)strMethodName withParameters:(NSDictionary*)dictParameter andImageData:(NSData *)imageData withCompletion:(void (^)(NSArray *data))completion WithFailure:(void (^)(NSString *error))failure
{
    NSString *strUrl = [NSString stringWithFormat:@"%@posts",serverURL];
    // NSString *strUrl=[NSString stringWithFormat:@"%@%@"serverURL,strMethodName];
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]init];
    
    if ([NSUSERDEFAULTS valueForKey:@"token"])
    {
        [param setValue:[NSUSERDEFAULTS valueForKey:@"token"] forKey:@"User-Token"];
    }
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[NSUSERDEFAULTS valueForKey:@"token"]  forHTTPHeaderField:@"User-Token"];
    
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:dictParameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        NSString *fileImg=[NSString stringWithFormat:@"giffplug.gif"];
        NSString *parameterNm=[NSString stringWithFormat:@"post_image"];
        
        [formData appendPartWithFileData:imageData name:parameterNm fileName:fileImg mimeType:@"image/gif"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject)
                                  {
                                      [CommonFunction removeActivityIndicator];
                                      if (responseObject != nil)
                                      {
                                          NSLog(@"%@",responseObject);
                                          
                                          if ([responseObject[@"msg"] isEqualToString:@"Post added successfully."])
                                          {
                                              [[[UIAlertView alloc]initWithTitle:@"Giffplug" message:@"Giff posted successfully" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                          }
                                          else
                                          {
                                              [[[UIAlertView alloc]initWithTitle:@"Giffplug Error" message:@"Giff could not be posted due to some error, try again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
                                          }
                                      }
                                  } failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  {
                                      [CommonFunction removeActivityIndicator];
                                      NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                      
                                      [[[UIAlertView alloc]initWithTitle:@"Giffplug Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
                                      
                                  }];
    
    
    [op start];
    
    
}


- (void)showSuggestionListForUsernames:(NSString *)usernameStr
{
    NSLog(@"show");
    [self getFollowingListAPIWithSuggestion:usernameStr];
}

- (void)removeSuggestionListForUsernames
{
     NSLog(@"hide");
    currentPage = 1;
    stopRequest = NO;
    isPageRefreshing = YES;
    isSuggesting = NO;
    [self removeTheTable];
}


#pragma mark - GetFollowingList API
-(void)getFollowingListAPIWithSuggestion:(NSString *)username;
{
    NSString *searchUserStr = username;
//    NSString *userID = [NSUSERDEFAULTS objectForKey:kUSER_SID];
    NSString *url = [NSString stringWithFormat:@"users/searchUserBykeyword/%@/%ld",searchUserStr,(long)currentPage];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    [CommonFunction showActivityIndicatorWithText:@""];
    
    __weak typeof(self) weakSelf = self;
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil){
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
//            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            [weakSelf removeSuggestionListForUsernames];
        }
        else if([operation.response statusCode]==200)
        {
            if (isSuggesting)
            {
                limit = [responseDict[@"limit"]integerValue];
                if ([responseDict[@"users"] isKindOfClass:[NSArray class]])
                {
                    NSArray *suggestedUser = responseDict[@"users"];
                    if (suggestedUser.count>0)
                    {
                        if (!arraySuggestedUsers)
                        {
                            arraySuggestedUsers = [[NSMutableArray alloc]init];
                        }
                        if (currentPage == 1)
                        {
                            [arraySuggestedUsers removeAllObjects];
                        }
                        [arraySuggestedUsers addObjectsFromArray:[suggestedUser mutableCopy]];
                        [weakSelf updateTheTable];
                        stopRequest = NO;
                    }
                    else
                    {
                        [weakSelf removeSuggestionListForUsernames];
                    }
                }
                else
                {
                    [weakSelf removeSuggestionListForUsernames];
                }
            }
            
            isPageRefreshing = NO;
        }
        else
        {
            if (currentPage>1)
            {
                currentPage--;
            }
            isPageRefreshing = NO;
//            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            [weakSelf removeSuggestionListForUsernames];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          
                                          if (currentPage>1)
                                          {
                                              currentPage--;
                                          }
                                          isPageRefreshing = NO;
                                          
                                          [weakSelf removeSuggestionListForUsernames];
                                      }];
}


- (void)updateTheTable
{
    if (!tableViewSuggestion)
    {
        tableViewSuggestion = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, MIN(250 *SCREEN_XScale, (50*SCREEN_XScale)*arraySuggestedUsers.count)) style:UITableViewStylePlain];
        [tableViewSuggestion registerNib:[UINib nibWithNibName:NSStringFromClass([UserSuggestCell class]) bundle:nil] forCellReuseIdentifier:kCellIdentifierSuggestUser];
        tableViewSuggestion.delegate=self;
        tableViewSuggestion.dataSource=self;
        tableViewSuggestion.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableViewSuggestion.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
        [self.view addSubview:tableViewSuggestion];
    }
    else
    {
        [tableViewSuggestion setFrame:CGRectMake(0, 0, self.view.frame.size.width, MIN(250*SCREEN_XScale, (50*SCREEN_XScale)*arraySuggestedUsers.count))];
    }
    [tableViewSuggestion reloadData];
    float factor = MIN(250*SCREEN_XScale, (50*SCREEN_XScale)*arraySuggestedUsers.count) / 2;
    tableViewSuggestion.center=CGPointMake(tableViewSuggestion.center.x, _captionTextView.superview.frame.origin.y - factor);
}

- (void)removeTheTable
{
    if (tableViewSuggestion) {
        tableViewSuggestion.delegate=nil;
        tableViewSuggestion.dataSource=nil;
        [tableViewSuggestion removeFromSuperview];
        tableViewSuggestion = nil;
    }
}

#pragma mark -
#pragma mark UITableView Delegates And Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f * SCREEN_XScale;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraySuggestedUsers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *cellIdentifier = [NSString stringWithFormat:@"cell%ld",(long)indexPath.row];
    UserSuggestCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifierSuggestUser];
//    if (!cell) {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
    NSString *strAvtarimg = arraySuggestedUsers[indexPath.row][@"avatar_image"];
    
    [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:strAvtarimg] placeholderImage:[UIImage imageNamed:@"default.jpg"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error)
        {
            cell.userImageView.contentMode=UIViewContentModeScaleToFill;
        }
        else
        {
            if ([[strAvtarimg pathExtension]isEqualToString:@"gif"])
            {
                cell.userImageView.contentMode=UIViewContentModeScaleAspectFill;
            }
            else
            {
                cell.userImageView.contentMode=UIViewContentModeScaleToFill;
            }
        }
    }];
    
    cell.labelUsername.text = arraySuggestedUsers[indexPath.row][@"username"];
    if ([arraySuggestedUsers[indexPath.row][@"name"] isKindOfClass:[NSString class]]) {
        NSString *name = arraySuggestedUsers[indexPath.row][@"name"];
        if (name.length>0)
        {
            cell.labelGeneralName.text = arraySuggestedUsers[indexPath.row][@"name"];
        }
        else
        {
            cell.labelGeneralName.text = @"";
            cell.constraintCenterUsername.constant = 0;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *name = arraySuggestedUsers[indexPath.row][@"username"];
    
    _captionTextView.text = [_captionTextView.text substringToIndex:_captionTextView.text.length-lastString.length];
    _captionTextView.text = [_captionTextView.text stringByAppendingString:[NSString stringWithFormat:@"@%@ ",name]];
//    _captionTextView.text = [_captionTextView.text stringByReplacingOccurrencesOfString:lastString withString:[NSString stringWithFormat:@"@%@ ",name]];
    
    [self removeSuggestionListForUsernames];
//    NSDictionary *attrs = @{ NSForegroundColorAttributeName : [UIColor cyanColor] };
//    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:name attributes:attrs];
    
    
}

#pragma mark - UIScrollViewDelegate Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (([tableViewSuggestion contentOffset].y + tableViewSuggestion.frame.size.height) >= [tableViewSuggestion contentSize].height)
    {
        if (stopRequest)
        {
            return;
        }
        NSLog(@"scrolled to bottom");
        if (!isPageRefreshing) {
            
            if(arraySuggestedUsers.count != limit)
            {
                return;
            }
            else
            {
                currentPage++;
            }
            
            isPageRefreshing = YES;
            [self getFollowingListAPIWithSuggestion:lastString];
        }
    }
}


@end

