//
//  EditUserProfileVC.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 06/02/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "EditUserProfileVC.h"
#import <QuartzCore/QuartzCore.h>
#import "CreateGiffFirstVC.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"
#import "UIImage+Resize.h"
#import "PhotoPickerController.h"


@interface EditUserProfileVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate>
{
    IBOutlet UIView *vwHud;
    IBOutlet UIView *vwAddress;
    IBOutlet UIView *vwBio;
    IBOutlet UIView *vwUsername;
    IBOutlet UIView *vwInImgVw;
    IBOutlet UIView *vwBacgroundImg;
    
    IBOutlet UIProgressView *prgresVw;
    
    IBOutlet UIScrollView *mainScrllVw;
    IBOutlet UIScrollView *scrllVwBgImage;
    
    IBOutlet UITextField *txtFldAddress;
    IBOutlet UITextField *txtUsername;
    IBOutlet UITextField *txtFldBio;
    
    IBOutlet UIImageView *imgbgVw;
    IBOutlet UIImageView *imgPatternImgVw;
    IBOutlet UIImageView *imPttrnImgVw1;
    IBOutlet UIImageView *imgEditAvtar;
    
//    IBOutlet UIButton *btnEditAvtarImg;
    
    __weak IBOutlet FLAnimatedImageView *avatarImageView;
    UIImage *croppedImage;
    NSInteger intBtnTag;
    
    BOOL isCroppedImageSet;
    BOOL isAvtarImageSet;
    
    float xScale;
    
    NSMutableArray *gifArray;
    BOOL isGifSelected;
    float animationDuration;
}
@end

@implementation EditUserProfileVC

#pragma mark - Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
            prgresVw.hidden=YES;
    [self setUpView];
    isAvtarImageSet=false;
    isCroppedImageSet=false;
    
    gifArray = [[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onGifselected:) name:@"gifSelected" object:nil];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (self.isMovingFromParentViewController)
    {
        [[NSNotificationCenter defaultCenter]removeObserver:self name:@"gifSelected" object:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)onGifselected:(NSNotification *)notification
{
    NSDictionary *gifDict = notification.object;
    [gifArray removeAllObjects];
    for (NSDictionary *dict in gifDict[@"image"])
    {
        UIImage *image = dict[@"image"];
        image = [image resizedImageToFitInSize:CGSizeMake(avatarImageView.frame.size.width*4, avatarImageView.frame.size.width*4) scaleIfSmaller:NO];
        [gifArray addObject:image];
    }
    avatarImageView.contentMode=UIViewContentModeScaleAspectFill;
    avatarImageView.image = nil;
    animationDuration = [gifDict[@"duration"]floatValue];
    [avatarImageView setImage:[UIImage animatedImageWithImages:gifArray duration:animationDuration]];
    isAvtarImageSet= YES;
    isGifSelected = YES;
}

-(void)setUpView
{
    [txtUsername setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [txtFldBio setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [txtFldAddress setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    vwBio.layer.cornerRadius=7.0f;
    vwBio.layer.borderColor=[UIColor colorWithRed:63.0f/255.0f green:64.0f/255.0f blue:64.0f/255.0f alpha:1].CGColor;
    vwBio.layer.borderWidth=2.0f;
    
    vwAddress.layer.cornerRadius=7.0f;
    vwAddress.layer.borderColor=[UIColor colorWithRed:63.0f/255.0f green:64.0f/255.0f blue:64.0f/255.0f alpha:1].CGColor;
    vwAddress.layer.borderWidth=2.0f;
    
    xScale=[[UIScreen mainScreen]bounds].size.width/320;
    
    vwUsername.layer.cornerRadius=7.0f;
    vwUsername.layer.borderColor=[UIColor colorWithRed:63.0f/255.0f green:64.0f/255.0f blue:64.0f/255.0f alpha:1].CGColor;
    vwUsername.layer.borderWidth=2.0f;
    
    UITapGestureRecognizer *tapGeture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapChangeAvatar:)];
    tapGeture.numberOfTapsRequired = 1;
    [avatarImageView addGestureRecognizer:tapGeture];
    
    avatarImageView.layer.cornerRadius=avatarImageView.frame.size.width/2*xScale;
    avatarImageView.clipsToBounds=YES;

//    btnEditAvtarImg.layer.cornerRadius=btnEditAvtarImg.frame.size.width/2*xScale;
//    btnEditAvtarImg.clipsToBounds=YES;
    
    UIView *txtFldVw=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 30)];
    txtUsername.leftViewMode=UITextFieldViewModeAlways;
    txtUsername.leftView=txtFldVw;
    txtFldAddress.autocorrectionType = UITextAutocorrectionTypeNo;
    txtFldBio.autocorrectionType = UITextAutocorrectionTypeNo;
    txtUsername.autocorrectionType = UITextAutocorrectionTypeNo;
    NSLog(@"%@",self.dictData);
    
    
    [imgbgVw sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[self.dictData valueForKey:@"main_background_image"]]]];
    
    
    
    NSString *strAvtarImg=[NSString stringWithFormat:@"%@",[self.dictData valueForKey:@"avatar_image"]];
    if (strAvtarImg.length>1) {
        if ([strAvtarImg.pathExtension isEqualToString:@"gif"])
        {
            avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        else
        {
            avatarImageView.contentMode = UIViewContentModeScaleToFill;
        }
        [avatarImageView sd_setImageWithURL:[NSURL URLWithString:strAvtarImg]];
//        [btnEditAvtarImg sd_setImageWithURL:[NSURL URLWithString:strAvtarImg] forState:UIControlStateNormal];
    }
    else
    {
        avatarImageView.contentMode = UIViewContentModeScaleToFill;
        [avatarImageView setImage:[UIImage imageNamed:@"default.jpg"]];
//        [btnEditAvtarImg setImage:[UIImage imageNamed:@"default.jpg"] forState:UIControlStateNormal];
    }
    
    
    if ([self isNotNull:self.dictData [@"name"]]) {
        [txtUsername setText:self.dictData [@"name"]];
    }
    else
    {
        [txtUsername setText:@""];
    }
    [txtFldAddress setText:self.dictData[@"address"]];
    [txtFldBio setText:self.dictData [@"bio"]];
    
    UIImage *backgroundImage = [[UIImage imageNamed:@"EP_cropperImg"] stretchableImageWithLeftCapWidth:0.5 topCapHeight:0];
    [imgPatternImgVw setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];
    [imPttrnImgVw1 setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];
    
    //Adding Tap Gesture on View
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleBtnClicked:)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];

    //Adding Tap Gesture on Image View
imgEditAvtar.userInteractionEnabled = YES;
    UITapGestureRecognizer *imgTapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapButtonCLicked:)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [imgEditAvtar addGestureRecognizer:imgTapGesture];

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [scrllVwBgImage setContentInset:UIEdgeInsetsMake(vwInImgVw.frame.size.height+vwInImgVw.frame.origin.y, 0.0, 0.0f, 0.0f)];
    
    [scrllVwBgImage setScrollEnabled:YES];
}


- (UIImage*) getSubImageFrom: (UIImage*) img WithRect: (CGRect) rect {
    
    NSLog(@"%@",NSStringFromCGRect(rect));
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // translated rectangle for drawing sub image
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, img.size.width, img.size.height);
    
    // clip to the bounds of the image context
    // not strictly necessary as it will get clipped anyway?
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    
    // draw image
    [img drawInRect:drawRect];
    
    // grab image
    UIImage* subImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return subImage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIScrollView Methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView==scrllVwBgImage) {
        isCroppedImageSet=YES;
    }
}
-(void)scrollToNormalView
{
    [self.view endEditing:YES];
    [mainScrllVw setContentOffset:CGPointZero];
    [mainScrllVw setScrollEnabled:NO];
}
-(void)scrollViewToCenterOfScreen:(UITextField *)textField cntainter:(UIView *)view
{
    [mainScrllVw setScrollEnabled:YES];
//    float difference;
//    if (mainScrllVw.contentSize.height == 600)
//        difference = 30.0f;
//    else
//        difference = 30.0f;
    CGFloat viewCenterY = textField.center.y+view.frame.origin.y;
    NSLog(@"center.y -- %f",textField.center.y);
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
    CGFloat avaliableHeight = applicationFrame.size.height - 20.0f;
    NSLog(@"avaliableHeight---%f",applicationFrame.size.height);
    
    CGFloat y = viewCenterY - (avaliableHeight / 2.0f)-50;
    if (y < 0)
        y = 0;
    NSLog(@"%f",y);
    [mainScrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [mainScrllVw setContentSize:CGSizeMake(320.0f, 615.0f)];
}
#pragma mark - UIScrllView Delegate Method

-(IBAction)singleBtnClicked:(id)sender
{
    [self scrollToNormalView];
}


#pragma mark - IBAction Button Methods
- (IBAction)bioInfoBtnClicked:(id)sender {
    [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Update your bio here." onViewController:self useAsDelegate:NO dismissBlock:nil];
}
- (IBAction)addressInfoBtnClicked:(id)sender {
        [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Update your location here." onViewController:self useAsDelegate:NO dismissBlock:nil];
}

- (void)onTapChangeAvatar:(UITapGestureRecognizer *)tapGesture;
{
    
    __weak typeof(self) weakSelf = self;
    
    intBtnTag=tapGesture.view.tag;
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Choose Image"
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Take From Camera"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                 
                                 UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"GifPlug"
                                                                                       message:@"Your device doesn't have camera"
                                                                                      delegate:nil
                                                                             cancelButtonTitle:@"OK"
                                                                             otherButtonTitles: nil];
                                 
                                 [myAlertView show];
                                 
                             }else
                             {
                                 
                                 AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
                                 switch (authStatus) {
                                     case AVAuthorizationStatusAuthorized:
                                     {
                                         UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                         picker.delegate = self;
                                         picker.allowsEditing = YES;
                                         picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                         [weakSelf presentViewController:picker animated:YES completion:NULL];
                                     }
                                         break;
                                     case AVAuthorizationStatusRestricted:
                                     {
                                         
                                     }
                                     case AVAuthorizationStatusDenied:
                                     {
                                         [self showCameraEnablePopup];
                                     }
                                         break;
                                     case AVAuthorizationStatusNotDetermined:
                                     {
                                         [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                                             if(granted){
                                                 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                                 picker.delegate = self;
                                                 picker.allowsEditing = YES;
                                                 picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                 [weakSelf presentViewController:picker animated:YES completion:NULL];
                                             } else {
                                                 [self showCameraEnablePopup];
                                             }
                                         }];
                                     }
                                         break;
                                         
                                     default:
                                         break;
                                 }
                             }
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Choose From Gallery"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied || [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted)
                                 {
                                     PhotoPickerController *photoPicker = [[PhotoPickerController alloc]initWithNibName:NSStringFromClass([PhotoPickerController class]) bundle:nil];
                                     [weakSelf presentViewController:photoPicker animated:YES completion:nil];
                                 }
                                 else
                                 {
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                     picker.delegate = self;
                                     picker.allowsEditing = YES;
                                     picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                     [weakSelf presentViewController:picker animated:YES completion:NULL];
                                 }
                             }];
    
    UIAlertAction* createGif = [UIAlertAction
                             actionWithTitle:@"Create Gif"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 CreateGiffFirstVC *gif=[[CreateGiffFirstVC alloc] initWithNibName:@"CreateGiffFirstVC" bundle:nil];
                                 gif.isOpenForProfilePurpose = YES;
                                 UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:gif];
                                 [weakSelf presentViewController:navController animated:YES completion:nil];
                             }];
    
    UIAlertAction* simpleCancle = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [alert addAction:createGif];
    [alert addAction:simpleCancle];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showCameraEnablePopup
{
    [CommonFunction showAlertWithTitle:@"GifPlug" message:@"GifPlug doesn't have permission to use the camera, please change privacy settings" onViewController:self withButtonsArray:@[@"OK", @"Settings"] dismissBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 1)
        {
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }
    }];
}

- (IBAction)editAvtarBtnClicked:(UIButton *)sender {
    intBtnTag=sender.tag;
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Choose Image"
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Take From Camera"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                 
                                 UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"GifPlug"
                                                                                       message:@"Device has no camera"
                                                                                      delegate:nil
                                                                             cancelButtonTitle:@"OK"
                                                                             otherButtonTitles: nil];
                                 
                                 [myAlertView show];
                                 
                             }else
                             {
                                 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                 picker.delegate = self;
                                 picker.allowsEditing = YES;
                                 picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                 
                                 [self presentViewController:picker animated:YES completion:NULL];
                                 
                             }
                             
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Choose From Gallery"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                 picker.delegate = self;
                                 picker.allowsEditing = YES;
                                 picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                 [self presentViewController:picker animated:YES completion:NULL];
                             }];
    
    UIAlertAction* simpleCancle = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [alert addAction:simpleCancle];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
-(IBAction)imageTapButtonCLicked:(id)sender
{
    [self onTapChangeAvatar:nil];
}

- (IBAction)editBackgrundBtnClicked:(id)sender {

    __weak typeof(self) weakSelf = self;
    intBtnTag=0;
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Choose Image"
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Take From Camera"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                 
                                 UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"GifPlug"
                                                                                       message:@"Your device doesn't have camera"
                                                                                      delegate:nil
                                                                             cancelButtonTitle:@"OK"
                                                                             otherButtonTitles: nil];
                                 
                                 [myAlertView show];
                                 
                             }else
                             {
                                 
                                 AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
                                 switch (authStatus) {
                                     case AVAuthorizationStatusAuthorized:
                                     {
                                         UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                         picker.delegate = self;
                                         picker.allowsEditing = YES;
                                         picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                         [weakSelf presentViewController:picker animated:YES completion:NULL];
                                     }
                                         break;
                                     case AVAuthorizationStatusRestricted:
                                     {
                                         
                                     }
                                     case AVAuthorizationStatusDenied:
                                     {
                                         [self showCameraEnablePopup];
                                     }
                                         break;
                                     case AVAuthorizationStatusNotDetermined:
                                     {
                                         [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                                             if(granted){
                                                 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                                 picker.delegate = self;
                                                 picker.allowsEditing = YES;
                                                 picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                 [weakSelf presentViewController:picker animated:YES completion:NULL];
                                             } else {
                                                 [self showCameraEnablePopup];
                                             }
                                         }];
                                     }
                                         break;
                                         
                                     default:
                                         break;
                                 }
                             }
                         }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Choose From Gallery"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied || [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusRestricted)
                                 {
                                     PhotoPickerController *photoPicker = [[PhotoPickerController alloc]initWithNibName:NSStringFromClass([PhotoPickerController class]) bundle:nil];
                                     [weakSelf presentViewController:photoPicker animated:YES completion:nil];
                                 }
                                 else
                                 {
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                     picker.delegate = self;
                                     picker.allowsEditing = YES;
                                     picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                     [weakSelf presentViewController:picker animated:YES completion:NULL];
                                 }
                             }];
    
    UIAlertAction* simpleCancle = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    [alert addAction:simpleCancle];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    

}
- (IBAction)settingBtnClicked:(id)sender {
    [CommonFunction alertTitle:@"" withMessage:@"This will be in next release."];
}


- (IBAction)doneBtnClicked:(id)sender
{
    [self scrollToNormalView];
    
    if (imgbgVw.image) {
        UIGraphicsBeginImageContext(vwBacgroundImg.bounds.size);
        
        [vwBacgroundImg.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        CGRect rect = CGRectMake(0,vwInImgVw.frame.size.height+2,[UIScreen mainScreen].bounds.size.width, vwBacgroundImg.frame.size.height-vwInImgVw.frame.size.height-2);
        CGImageRef imageRef = CGImageCreateWithImageInRect([viewImage CGImage], rect);
        

        if (isCroppedImageSet) {
        croppedImage = [UIImage imageWithCGImage:imageRef];
        }
        else
        {
        croppedImage = nil;
        }
        CFRelease(imageRef);
        if (!isAvtarImageSet) {
            imgEditAvtar=nil;
        }
    }

    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtUsername.text],@"name",[CommonFunction trimSpaceInString:txtFldBio.text],@"bio",[CommonFunction trimSpaceInString:txtFldAddress.text],@"address", nil];
    
    
    [CommonFunction showActivityIndicatorWithText:@""];
    [self uploadImageBySessionUserMethod:@"" withParameters:param withCompletion:^(NSArray *data) {

        dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@",data);
        });

    } WithFailure:^(NSString *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%@",error);
        });
    }];
    
  }

-(IBAction)closeBtnClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITextField Delegate Methods
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == txtUsername )
    {
        if (textField.text.length >= 20 && range.length == 0)
            return NO;
    }
    else if (textField == txtFldBio)
    {
        if(textField.text.length >= 50 && range.length == 0)
        return NO;
        
    }
    else if (textField==txtFldAddress)
    {
        if (textField.text.length >= 20 && range.length == 0)
            return NO;
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==txtUsername) {
        [self scrollViewToCenterOfScreen:textField cntainter:vwUsername];
    }
    else if(textField==txtFldBio)
    {
        [self scrollViewToCenterOfScreen:textField cntainter:vwBio];
    }
    else
    {
        [self scrollViewToCenterOfScreen:textField cntainter:vwAddress];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (txtUsername==textField) {
        [txtFldBio becomeFirstResponder];
    }
    else if (txtFldBio==textField)
    {
        [txtFldAddress becomeFirstResponder];
            }
    else if (txtFldAddress==textField)
    {
        [self scrollToNormalView];

    }
    return YES;
}

#pragma mark - UIImage Controller Delegate Methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    //NSLog(@"%@",info);
    if (intBtnTag==1) {
        [avatarImageView setImage:[info objectForKey:UIImagePickerControllerEditedImage]];
//        [btnEditAvtarImg setImage:[info objectForKey:UIImagePickerControllerEditedImage] forState:UIControlStateNormal];
        isAvtarImageSet=YES;
        isGifSelected = NO;
        avatarImageView.contentMode=UIViewContentModeScaleToFill;
    }
    else
    {
            imgbgVw.image=[info objectForKey:UIImagePickerControllerEditedImage];
        [scrllVwBgImage setContentOffset:CGPointZero];
        isCroppedImageSet=YES;
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - WebService API
-(void)uploadImageBySessionUserMethod:(NSString *)strMethodName withParameters:(NSDictionary*)dictParameter withCompletion:(void (^)(NSArray *data))completion WithFailure:(void (^)(NSString *error))failure
{
    NSString *strUrl = [NSString stringWithFormat:@"%@users/updateProfile",serverURL];
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]init];
    
    if ([NSUSERDEFAULTS valueForKey:@"token"])
    {

         [param setValue:[NSUSERDEFAULTS valueForKey:@"token"] forKey:@"User-Token"];
    }
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:[NSUSERDEFAULTS valueForKey:@"token"]  forHTTPHeaderField:@"User-Token"];
    
    AFHTTPRequestOperation *op = [manager POST:@"" parameters:dictParameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {

        NSLog(@"%@",croppedImage);
        
        if (croppedImage) {
            NSData *imageData = UIImageJPEGRepresentation(croppedImage,0.7f);
            
            NSString *fileImg=[NSString stringWithFormat:@"profileImg.jpg"];
            NSString *parameterNm=[NSString stringWithFormat:@"background_image"];
            
            [formData appendPartWithFileData:imageData name:parameterNm fileName:fileImg mimeType:@"image/jpeg"];
        }
        
        if (avatarImageView.image && isAvtarImageSet) {
            
            NSString *fileImg1=[NSString stringWithFormat:@"profileImg.jpg"];
            NSString *parameterNm1=[NSString stringWithFormat:@"avatar_image"];
            if (isGifSelected)
            {
                fileImg1=[NSString stringWithFormat:@"profileImg.gif"];
                NSURL *gifUrl =[CommonFunction createGiffFromImageArray:gifArray andDelayTime:animationDuration/gifArray.count isProfileUpload:YES];
                
                NSData *gifData = [NSData dataWithContentsOfURL:gifUrl];
                
                [formData appendPartWithFileData:gifData name:parameterNm1 fileName:fileImg1 mimeType:@"image/gif"];
            }
            else
            {
                NSData *imageData1 = UIImageJPEGRepresentation(avatarImageView.image,0.7f);
                NSLog(@"0.8 %lu",(unsigned long)imageData1.length);
                
                
                
                [formData appendPartWithFileData:imageData1 name:parameterNm1 fileName:fileImg1 mimeType:@"image/jpeg"];
            }
            
        }
        if(imgbgVw.image && croppedImage)
        {
            NSData *imageData1 = UIImageJPEGRepresentation(imgbgVw.image,0.7f);
            NSLog(@"0.8 %lu",(unsigned long)imageData1.length);
            
            NSString *fileImg1=[NSString stringWithFormat:@"main_background_image.jpg"];
            NSString *parameterNm1=[NSString stringWithFormat:@"main_background_image"];
            
            [formData appendPartWithFileData:imageData1 name:parameterNm1 fileName:fileImg1 mimeType:@"image/jpeg"];
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject)
                                  {
                                      [CommonFunction removeActivityIndicator];
//                                      NSError* error;
                                      
                                      if (responseObject != nil)
                                      {

                                          NSDictionary *jsonDic = (NSDictionary *)responseObject;
                                          NSLog(@"Profile  JSON: %@",jsonDic);
                                          [self dismissViewControllerAnimated:YES completion:nil];
                                      }
                                  } failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  {
                                 [CommonFunction removeActivityIndicator];
                                      NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                      
                                  }];
    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        [vwHud setFrame:self.view.frame];
        [self.view addSubview:vwHud];
        prgresVw.hidden=NO;
       prgresVw.progress=(float)totalBytesWritten/(float)totalBytesExpectedToWrite;
        isAvtarImageSet=NO;
        isCroppedImageSet=NO;

    }];
    
    
    [op start];
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
