//
//  UpdateSettingVC.m
//  GiffPlugApp
//
//  Created by Santosh on 02/06/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "UpdateSettingVC.h"

@interface UpdateSettingVC ()
{
    NSMutableDictionary *settingsDict;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@property (weak, nonatomic) IBOutlet UITextField *textFieldUpdate;
- (IBAction)onUpdateSetting:(UIButton *)sender;
- (IBAction)onBackAction:(id)sender;
@end

@implementation UpdateSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    settingsDict = [[NSUSERDEFAULTS objectForKey:@"settings"]mutableCopy];
    [self updateView];
    UITapGestureRecognizer *gesture= [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapGesture)];
    gesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:gesture];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.textFieldUpdate becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.textFieldUpdate resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onTapGesture
{
    [self.textFieldUpdate resignFirstResponder];
}

- (void)updateView
{
    NSString *text = nil;
    if ([self.updateType isEqualToString:@"email"])
    {
        self.labelTitle.text = @"Update Email";
        self.textFieldUpdate.keyboardType = UIKeyboardTypeEmailAddress;
        text = settingsDict[@"email"];
    }
    else if ([self.updateType isEqualToString:@"password"])
    {
        self.labelTitle.text = @"Update Password";
        self.textFieldUpdate.secureTextEntry = YES;
        self.textFieldUpdate.keyboardType = UIKeyboardTypeASCIICapable;
        text = @"";
    }
    else if ([self.updateType isEqualToString:@"phone"])
    {
        self.textFieldUpdate.tag = 1000;
        self.labelTitle.text = @"Update Number";
        self.textFieldUpdate.keyboardType = UIKeyboardTypePhonePad;
        text = settingsDict[@"phone"];
    }
    else if ([self.updateType isEqualToString:@"terms"])
    {
        self.labelTitle.text = @"Terms & Conditions";
    }
    else if ([self.updateType isEqualToString:@"privacy"])
    {
        self.labelTitle.text = @"Privacy";
    }
    if (text && text.length>0)
    {
        self.textFieldUpdate.text = text;
    }
    self.textFieldUpdate.placeholder = self.labelTitle.text;
    [self.textFieldUpdate setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
}

- (IBAction)onUpdateSetting:(UIButton *)sender {
    
    NSString *updateText = [self.textFieldUpdate.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (updateText.length==0)
    {
        return;
    }
    if ([self.updateType isEqualToString:@"email"])
    {
        if ([updateText isEqualToString:settingsDict[@"email"]])
        {
            [self onBackAction:nil];
        }
        else
        {
            if (![CommonFunction IsValidEmail:updateText])
            {
                [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Please enter a valid email." onViewController:self useAsDelegate:NO dismissBlock:nil];
                return;
            }
            sender.enabled=NO;
            [self updateUserSettingsForType:self.updateType andUpdateText:updateText];
//            [settingsDict setValue:updateText forKey:@"email"];
//            [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
//            [NSUSERDEFAULTS synchronize];
        }
    }
    else if ([self.updateType isEqualToString:@"password"])
    {
        if (updateText.length<6)
        {
            [CommonFunction showAlertWithTitle:@"GifPlug" message:@"Please enter minimum 6 digit password." onViewController:self useAsDelegate:NO dismissBlock:nil];
        }
        else
        {
            sender.enabled=NO;
            [self updateUserSettingsForType:self.updateType andUpdateText:updateText];
        }
    }
    else if ([self.updateType isEqualToString:@"phone"])
    {
        if ([updateText isEqualToString:settingsDict[@"phone"]])
        {
            [self onBackAction:nil];
        }
        else
        {
            sender.enabled=NO;
            [self updateUserSettingsForType:self.updateType andUpdateText:updateText];
//            [settingsDict setValue:updateText forKey:@"phone"];
//            [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
//            [NSUSERDEFAULTS synchronize];
        }
    }
}

-(void)updateUserSettingsForType:(NSString *)updateType andUpdateText:(NSString *)updateText
{
    [CommonFunction showActivityIndicatorWithText:@""];
    
    NSString *url = [NSString stringWithFormat:@"users/updateSettings"];
    
    NSMutableDictionary *header =[NSMutableDictionary dictionaryWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:updateText,updateType, nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [(UIButton*)[weakSelf.view viewWithTag:101] setEnabled:YES];
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil){
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            if ([updateType isEqualToString:@"email"])
            {
                [settingsDict setValue:updateText forKey:@"email"];
                [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                [NSUSERDEFAULTS synchronize];
            }
            else if ([updateType isEqualToString:@"password"])
            {
                
            }
            else if ([updateType isEqualToString:@"phone"])
            {
                [settingsDict setValue:updateText forKey:@"phone"];
                [NSUSERDEFAULTS setObject:settingsDict forKey:@"settings"];
                [NSUSERDEFAULTS synchronize];
            }
            [self onBackAction:nil];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [(UIButton*)[weakSelf.view viewWithTag:101] setEnabled:YES];
                                          [CommonFunction removeActivityIndicator];
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                      }];
}

- (IBAction)onBackAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag != 1000)
    {
        return YES;
    }
    int length = (int)[self getLength:textField.text];
    //NSLog(@"Length  =  %d ",length);
    
    if(length == 10)
    {
        if(range.length == 0)
            return NO;
    }
    
    if(length == 3)
    {
        NSString *num = [self formatNumber:textField.text];
        textField.text = [NSString stringWithFormat:@"(%@) ",num];
        
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
    }
    else if(length == 6)
    {
        NSString *num = [self formatNumber:textField.text];
        //NSLog(@"%@",[num  substringToIndex:3]);
        //NSLog(@"%@",[num substringFromIndex:3]);
        textField.text = [NSString stringWithFormat:@"(%@) %@-",[num  substringToIndex:3],[num substringFromIndex:3]];
        
        if(range.length > 0)
            textField.text = [NSString stringWithFormat:@"(%@) %@",[num substringToIndex:3],[num substringFromIndex:3]];
    }
    
    return YES;
}

- (NSString *)formatNumber:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    NSLog(@"%@", mobileNumber);
    
    int length = (int)[mobileNumber length];
    if(length > 10)
    {
        mobileNumber = [mobileNumber substringFromIndex: length-10];
        NSLog(@"%@", mobileNumber);
        
    }
    
    return mobileNumber;
}

- (int)getLength:(NSString *)mobileNumber
{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    
    int length = (int)[mobileNumber length];
    
    return length;
}
@end
