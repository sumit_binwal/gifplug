//
//  LoginVC.m
//  GiffPlugApp
//
//  Created by Kshitij Godara on 08/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "LoginVC.h"
#import "SignUpVC.h"
#import "FollowUnFollowScreen.h"
#import "GuidedContentScreen.h"
#import "UserProfileVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIImageView+WebCache.h"
#import "UIImage+GIF.h"
#import "ValidateUserNameVC.h"
#import "ForgotPasswordVC.h"


@interface LoginVC ()<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    __weak IBOutlet UIView *vwVideoBg;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    IBOutlet UIImageView *imgVwRedioBtn;
    IBOutlet UIButton *btnRememberMe;
    
//    NSTimer *timer;
    CMTime vdoCurrentTime;
}
//@property (strong,nonatomic)id timeObserver;
//@property(strong,nonatomic)AVAssetImageGenerator *imageGenerator;
//@property (strong, nonatomic) IBOutlet UIImageView *containerThumbnailView;
@property(strong,nonatomic)AVPlayer *avPlayer;
@property(strong,nonatomic)AVPlayerItem *avPlayerItem;
@property(strong,nonatomic)AVAsset *avAsset;
@property(strong,nonatomic)AVPlayerLayer *avPlayerLayer;
@end

@implementation LoginVC


#pragma mark - hide status bar

- (BOOL)prefersStatusBarHidden
{
    
    return NO;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self setUpView];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    
    NSLog(@"%@",[NSUSERDEFAULTS valueForKey:kLOGINUSERNAME]);
    NSLog(@"%@",[NSUSERDEFAULTS valueForKey:kUSERNAME]);

    if ([self isNotNull:[NSUSERDEFAULTS valueForKey:kLOGINUSERNAME]]&&[self isNotNull:[NSUSERDEFAULTS valueForKey:kLOGINPASSWORD]]) {
        txtEmail.text=[NSUSERDEFAULTS valueForKey:kLOGINUSERNAME];
        txtPassword.text=[NSUSERDEFAULTS valueForKey:kLOGINPASSWORD];
        btnRememberMe.tag=1;
        [imgVwRedioBtn setImage:[UIImage imageNamed:@"login_RadioSelect"]];
    }
    else
    {
        txtEmail.text=@"";
        txtPassword.text=@"";
    }
    [NSUSERDEFAULTS setObject:@"0" forKey:kISCATEGORYSUBMITTED];
    [NSUSERDEFAULTS setObject:@"0" forKey:kISFOLLOWINGSUBMITTED];
    [NSUSERDEFAULTS synchronize];
    
    if (self.avPlayer)
    {
        [self.avPlayer play];
    }
    else
    {
        [self playVideo];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appEnteredBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avPlayer currentItem]];

    
}

-(void)viewWillDisappear:(BOOL)animated
{
//    [self.avPlayer removeTimeObserver:self.timeObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [self.avPlayer pause];
    [super viewWillDisappear:animated];
}




#pragma mark - UITextFiled Validation Method

-(BOOL)isTextFieldValidate
{
    if (![CommonFunction isValueNotEmpty:txtEmail.text]) {
        [CommonFunction alertTitle:@"" withMessage:@"Please enter username."];
        return NO;
    }
    else if (![CommonFunction isValueNotEmpty:txtPassword.text])
    {
        [CommonFunction alertTitle:@"" withMessage:@"Please enter password."];
        return NO;
    }
    else
    {
        return YES;
    }
}

#pragma mark - SetupView Method

-(void)setUpView
{
    //Hide Navigation Bar
    [self setNavigationBarProperties];
    
    [txtEmail setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [txtPassword setValue:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapBtnClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    wbServiceCount=1;
   
}

#pragma mark - Play Video In Av Player Method

-(void)playVideo
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *filepath1 = [[NSBundle mainBundle] pathForResource:@"newlogin" ofType:@"mov"];
        NSURL *fileURL1 = [NSURL fileURLWithPath:filepath1];
        
        weakSelf.avPlayer = [AVPlayer playerWithURL:fileURL1]; //
        weakSelf.avAsset = [AVAsset assetWithURL:fileURL1];
        //    _imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:self.avAsset];
        //    _imageGenerator.appliesPreferredTrackTransform = YES;
        weakSelf.avPlayerLayer = [AVPlayerLayer layer];
        
        [weakSelf.avPlayerLayer setPlayer:self.avPlayer];
        [weakSelf.avPlayerLayer setFrame:[UIScreen mainScreen].bounds];
        //[layer setBackgroundColor:[UIColor redColor].CGColor];
        [weakSelf.avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        
        [vwVideoBg.layer addSublayer:self.avPlayerLayer];
        [weakSelf.avPlayer play];
        
        weakSelf.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    });
}
#pragma mark - AvPlayer Background Forground Even Method

-(void) appEnteredForeground {
    // Application Goes In Forground and play video
    [self.avPlayer seekToTime:vdoCurrentTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
        [self.avPlayer play];
    }];
}

-(void) appEnteredBackground {
    // Application Goes In Background
    [self.avPlayer pause];
    AVPlayerItem *currentItem = self.avPlayer.currentItem;
    
    vdoCurrentTime = currentItem.currentTime;
}
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    // video did reach end and repeate video
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}



-(void)singleTapBtnClicked
{
    [self.view endEditing:YES];
}

#pragma mark - Navigation Items

-(void)setNavigationBarProperties
{
    [CommonFunction hideNavigationBarFromController:nil];
}

#pragma mark - IBActions
- (IBAction)rememberBtnClicked:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0) {
        [imgVwRedioBtn setImage:[UIImage imageNamed:@"login_RadioSelect"]];
        btn.tag=1;
    }
    else if (btn.tag==1)
    {
         [imgVwRedioBtn setImage:[UIImage imageNamed:@"login_radioUnselect"]];
        btn.tag=0;
    }
}

- (IBAction)forgotBtnClicked:(id)sender {
    
    ForgotPasswordVC *fufs=[[ForgotPasswordVC alloc]initWithNibName:@"ForgotPasswordVC" bundle:nil];
    [self presentViewController:fufs animated:YES completion:nil];
}
- (IBAction)signUpBtnClicked:(id)sender {

  
    SignUpVC *sgvc=[[SignUpVC alloc]initWithNibName:@"SignUpVC" bundle:nil];
    [self.navigationController pushViewController:sgvc animated:YES];
}
- (IBAction)facebookBtnClicked:(id)sender {

    __weak typeof(self) weakSelf = self;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:weakSelf
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {

         if (result.token)
         {
             [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"about, first_name, last_name, email"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id res, NSError *error)
              {
                  NSLog(@"%@",res);
                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
                  if (!result || result.isCancelled) {
                  }
                  else if (!error) {
                      NSLog(@"%@",res);
                      [weakSelf fbLoginAPI:res];
                      NSLog(@"the EMail = %@", [res objectForKey:@"username"]);
                  }
                  else
                  {
                      NSLog(@"%@", [error localizedDescription]);
                  }}];
         }
     }];

    

    
    

    
//    void(^loginHandler)(FBSDKLoginManagerLoginResult *result, NSError *error) = ^(FBSDKLoginManagerLoginResult *result, NSError *error){
//        NSLog(@"%@",result);
////        [result valueForKey:@"first_name"];
//
////        UIAlertController *alertController;
////        if (error) {
////            //alertController = [AlertControllerUtility alertControllerWithTitle:@"Login Fail"
////                                                                       message:[NSString stringWithFormat:@"Login fail with error: %@", error]];
////        } else if (!result || result.isCancelled) {
////            //alertController = [AlertControllerUtility alertControllerWithTitle:@"Login Cancelled"
////                                                                       message:@"User cancelled login"];
////        } else {
/////           // alertController = [AlertControllerUtility alertControllerWithTitle:@"Login Success"
////                                                                       message:[NSString stringWithFormat:@"Login success with granted permission: %@", [[result.grantedPermissions allObjects] componentsJoinedByString:@" "]] ];
//////        }
//////        [self presentViewController:alertController animated:YES completion:nil];
//    };
//    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
//    if (![FBSDKAccessToken currentAccessToken]) {
//        [loginManager logInWithReadPermissions: @[@"public_profile", @"user_friends"]
//                            fromViewController:self
//                                       handler:loginHandler];
//    } else {
//        [loginManager logOut];
//        //UIAlertController *alertController = [AlertControllerUtility alertControllerWithTitle:@"Logout" message:@"Logout"];
//        //[self presentViewController:alertController animated:YES completion:nil];
//    }
//    
//    
    
   }

- (IBAction)LoginBtnPressed:(id)sender
{
    [self.view endEditing:YES];
    NSLog(@"%ld",(long)btnRememberMe.tag);
//    [CommonFunction changeRootViewController:YES];
    if ([self isTextFieldValidate]) {
        //Set Tab bar as root view controller
        if ([CommonFunction reachabiltyCheck]) {
         //   [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

            [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
            [self loginAPI];
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
}


#pragma mark - UITextField Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
 if(textField==txtEmail)
 {
     [txtPassword becomeFirstResponder];
 }
    else if (textField==txtPassword)
    {
        [txtPassword resignFirstResponder];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@" "] )
    {
        return NO;
    }
    else if (txtEmail==textField)
    {
            return [textField textInputMode] != nil;
    }
    return YES;
}


#pragma mark - Memory Managment

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - WebService API
-(void)loginAPI
{
    
    NSString *url = [NSString stringWithFormat:@"users/login"];
    //http://192.168.0.22:8353/v1/users/login

    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunction trimSpaceInString:txtEmail.text],@"username",[CommonFunction trimSpaceInString:txtPassword.text],@"password",@"ios",@"device_type",appDeviceToken,@"device_token", nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;


        APPDELEGATE.window.userInteractionEnabled=YES;
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
            
        {
           //[CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            [NSUSERDEFAULTS removeObjectForKey:kLOGINPASSWORD];
            [NSUSERDEFAULTS removeObjectForKey:kLOGINUSERNAME];
            
            [NSUSERDEFAULTS setObject:txtEmail.text forKey:kUSERID];
            if (btnRememberMe.tag==1) {
                [NSUSERDEFAULTS setObject:txtEmail.text forKey:kLOGINUSERNAME];
                [NSUSERDEFAULTS setObject:txtPassword.text forKey:kLOGINPASSWORD];

            }

            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"userId"] forKey:kUSER_SID];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"username"] forKey:kUSERNAME];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"token"] forKey:kUSERTOKEN];
            [NSUSERDEFAULTS setObject:[responseDict objectForKey:@"categories"] forKey:kFOLLOWINGCATEGORY];
            [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"hasCategory"] stringValue] forKey:kISCATEGORYSUBMITTED];
            [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"hasFollowing"] stringValue] forKey:kISFOLLOWINGSUBMITTED];
            
            [NSUSERDEFAULTS synchronize];
            
            [APPDELEGATE.communication setUpCommunicationStream];
            
            if ([[NSUSERDEFAULTS valueForKey:kISCATEGORYSUBMITTED] isEqualToString:@"0"]) {
                GuidedContentScreen *gcS=[[GuidedContentScreen alloc]initWithNibName:@"GuidedContentScreen" bundle:nil];
                [weakSelf.navigationController pushViewController:gcS animated:YES];
            }
            else if ([[NSUSERDEFAULTS valueForKey:kISFOLLOWINGSUBMITTED] isEqualToString:@"0"])
            {
                FollowUnFollowScreen *fufs=[[FollowUnFollowScreen alloc]initWithNibName:@"FollowUnFollowScreen" bundle:nil];
                NSLog(@"%@",[NSUSERDEFAULTS objectForKey:kFOLLOWINGCATEGORY]);
                [weakSelf.navigationController pushViewController:fufs animated:YES];
            }
            else
            {
            [CommonFunction changeRootViewController:YES];
            [[UITabBar appearance] setTintColor:[UIColor colorWithRed:255.0f/255.0f green:33.0f/255.0f blue:87.0f/255.0f alpha:1]];
            }

        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

                                                  [weakSelf loginAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}


-(void)fbLoginAPI:(NSMutableDictionary *)dataDict
{
    [NSUSERDEFAULTS removeObjectForKey:kLOGINPASSWORD];
    [NSUSERDEFAULTS removeObjectForKey:kLOGINUSERNAME];
    [NSUSERDEFAULTS synchronize];
    NSString *url = [NSString stringWithFormat:@"users/socialSignup"];
    //http://192.168.0.22:8353/v1/users/socialSignup
    NSString *strEmail=[dataDict valueForKey:@"email"];
    if (strEmail.length<1) {
        strEmail=@"";
    }
    NSString *strUsername=[NSString stringWithFormat:@"%@_%@",[dataDict valueForKey:@"first_name"],[dataDict valueForKey:@"last_name"]];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type", nil];
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithObjectsAndKeys:strUsername,@"username",[dataDict valueForKey:@"id"],@"social_id",[dataDict valueForKey:@"first_name"],@"name",@"facebook",@"social_type",@"ios",@"device_type",appDeviceToken,@"device_token",strEmail,@"email", nil];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    __weak typeof(self) weakSelf = self;
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:url withParameters:param withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([operation.response statusCode]==200)
            
        {
            //[CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];

     
            NSLog(@"%@",[NSUSERDEFAULTS objectForKey:kLOGINUSERNAME]);

            [NSUSERDEFAULTS setObject:txtEmail.text forKey:kUSERID];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"userId"] forKey:kUSER_SID];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"username"] forKey:kUSERNAME];
            [NSUSERDEFAULTS setObject:[responseDict valueForKey:@"token"] forKey:kUSERTOKEN];
            [NSUSERDEFAULTS setObject:[responseDict objectForKey:@"categories"] forKey:kFOLLOWINGCATEGORY];
            [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"hasCategory"] stringValue] forKey:kISCATEGORYSUBMITTED];
            [NSUSERDEFAULTS setObject:[[responseDict valueForKey:@"hasFollowing"] stringValue] forKey:kISFOLLOWINGSUBMITTED];
            
            [NSUSERDEFAULTS synchronize];
            
            [APPDELEGATE.communication setUpCommunicationStream];
            
            if ([[NSUSERDEFAULTS valueForKey:kISCATEGORYSUBMITTED] isEqualToString:@"0"]) {
                GuidedContentScreen *gcS=[[GuidedContentScreen alloc]initWithNibName:@"GuidedContentScreen" bundle:nil];
                [weakSelf.navigationController pushViewController:gcS animated:YES];
            }
            else if ([[NSUSERDEFAULTS valueForKey:kISFOLLOWINGSUBMITTED] isEqualToString:@"0"])
            {
                FollowUnFollowScreen *fufs=[[FollowUnFollowScreen alloc]initWithNibName:@"FollowUnFollowScreen" bundle:nil];
                NSLog(@"%@",[NSUSERDEFAULTS objectForKey:kFOLLOWINGCATEGORY]);
                [weakSelf.navigationController pushViewController:fufs animated:YES];
            }
            else
            {
                [CommonFunction changeRootViewController:YES];
                [[UITabBar appearance] setTintColor:[UIColor colorWithRed:255.0f/255.0f green:33.0f/255.0f blue:87.0f/255.0f alpha:1]];
            }

            
        }
        else
        {
            if([[responseDict objectForKey:@"msg"]isEqualToString:@"EMAIL_ALREADY_EXIST"])
            {
                UIAlertController *alert=[UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Your Facebook email address, %@ , already has an account with GifPlug",strEmail ]message:@"" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *newAccount=[UIAlertAction actionWithTitle:@"New Account" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    SignUpVC *suvc=[[SignUpVC alloc]init];
                    [weakSelf.navigationController pushViewController:suvc animated:YES];
                }];
                UIAlertAction *lgin=[UIAlertAction actionWithTitle:@"Log in" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                [alert addAction:newAccount];
                [alert addAction:lgin];
                [weakSelf presentViewController:alert animated:YES completion:nil];
            }
            if([[responseDict objectForKey:@"msg"]isEqualToString:@"USERNAME_ALREADY_EXIST"])
            {
                [CommonFunction showAlertWithTitle:@"GifPlug" message:@"This username is already taken by another user please try new." onViewController:weakSelf useAsDelegate:NO dismissBlock:^{
                    ValidateUserNameVC *vuvc=[[ValidateUserNameVC alloc]init];
                    vuvc.dataDict=dataDict;
                    [weakSelf.navigationController pushViewController:vuvc animated:YES];
                }];

            }
            else
            {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
            }

        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              
                                              [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;

                                                  [weakSelf loginAPI];
                                              }
                                              else
                                              {
                                                  [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;

                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                              
                                          }
                                          
                                      }];
}

@end


//#import <objc/runtime.h>
//
//@interface UITextField (longPress)
//
//- (void)cat_addGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer;
//
//@end
//
//@implementation UITextField (longPress)
//
//+ (void)load
//{
//    static dispatch_once_t once_token;
//    dispatch_once(&once_token,  ^{
//        SEL oldThumbRect = @selector(addGestureRecognizer:);
//        SEL newThumbRect = @selector(cat_addGestureRecognizer:);
//        Method originalMethod = class_getInstanceMethod(self, oldThumbRect);
//        Method extendedMethod = class_getInstanceMethod(self, newThumbRect);
//        method_exchangeImplementations(originalMethod, extendedMethod);
//    });
//}
//
//- (void)cat_addGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
//{
//    if ([self isKindOfClass:[UITextField class]])
//    {
//        if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
//        {
//            gestureRecognizer.enabled = NO;
//        }
//        [self cat_addGestureRecognizer:gestureRecognizer];
//    }
//    return;
//}
//
//@end
