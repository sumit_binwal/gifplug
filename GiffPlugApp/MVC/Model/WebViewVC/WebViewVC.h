//
//  WebViewVC.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 08/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewVC : UIViewController
@property(nonatomic,strong)NSString *strWbUrl;
@property (nonatomic,strong)NSURL *webURL;
@end
