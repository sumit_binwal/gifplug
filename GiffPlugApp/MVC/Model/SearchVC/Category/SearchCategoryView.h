//
//  SearchCategoryView.h
//  GiffPlugApp
//
//  Created by Santosh on 28/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CategorySearchDelegate;

@interface SearchCategoryView : UIView<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,weak)id<CategorySearchDelegate> delegate;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionHashTags;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionCategories;

-(void)getCategoryList;
@end

@protocol CategorySearchDelegate <NSObject>

- (void)categorySearchViewDidSelectedCategoryWithName:(NSString *)categoryName andCategoryTag:(NSString *)categoryTag;
@end
