//
//  SearchCategoryView.m
//  GiffPlugApp
//
//  Created by Santosh on 28/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "SearchCategoryView.h"
#import "CategoryCell.h"
#import "TrendingCell.h"
#import "KIHTTPRequestOperationManager.h"
@interface SearchCategoryView ()
{
    NSMutableArray *categoriesArray;
    NSMutableArray *trendingArray;
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UILabel *labelEmptyHashtag;
@end

@implementation SearchCategoryView


- (void)dealloc
{
    NSLog(@"dealloced called for Search Category VIEW...................");
}

- (void)awakeFromNib
{
    categoriesArray = [[NSMutableArray alloc]init];
    
    trendingArray = [[NSMutableArray alloc]init];
    
//    refreshControl=[[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
//    [refreshControl setTintColor:[UIColor whiteColor]];
//    [refreshControl setBackgroundColor:[UIColor blackColor]];
//    
//    [self.collectionCategories addSubview:refreshControl];
//    [refreshControl setAutoresizingMask:(UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin)];
//    
//    [[self.collectionCategories.subviews objectAtIndex:0] setFrame:CGRectMake(0, 0, 20, 30)];
    
    [self.collectionHashTags registerNib:[UINib nibWithNibName:NSStringFromClass([TrendingCell class]) bundle:nil] forCellWithReuseIdentifier:kCellTrendingIdentifier];
    [self.collectionCategories registerNib:[UINib nibWithNibName:NSStringFromClass([CategoryCell class]) bundle:nil] forCellWithReuseIdentifier:kCategoryCellIdentifier];
    self.collectionCategories.contentInset = UIEdgeInsetsMake(10, 0, 10, 0);
    
//    [self getCategoryList];

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 101)
    {
        NSString *string = [NSString stringWithFormat:@"#%@",trendingArray[indexPath.row]];
        CGRect textRect = [string boundingRectWithSize:CGSizeMake(20000.0f, collectionView.frame.size.height)
                                             options:NSStringDrawingUsesFontLeading
                                          attributes:@{NSFontAttributeName:[UIFont fontWithName:@"NexaLight" size:17.0f]}
                                             context:nil];
        return CGSizeMake(textRect.size.width+1, collectionView.frame.size.height);
    }
    return CGSizeMake(142.0f *SCREEN_XScale, 167.0f *SCREEN_XScale);
}

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    return 1.0;
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
//    return 1.0;
//}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView.tag == 101)
    {
        return UIEdgeInsetsMake(0,10,0,10);
    }
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,12,0,12);  // top, left, bottom, right
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == 101)
    {
        return trendingArray.count;
    }
    return categoriesArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 101)
    {
        TrendingCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellTrendingIdentifier forIndexPath:indexPath];
        cell.hashtagName.text = [NSString stringWithFormat:@"#%@",trendingArray[indexPath.row]];
        return cell;
    }
    CategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCategoryCellIdentifier forIndexPath:indexPath];
    cell.categoryName.text = categoriesArray[indexPath.row][@"category_title"];
    
    NSString *imageUrl = nil;
    if (categoriesArray[indexPath.row][@"post_image"])
    {
        imageUrl = categoriesArray[indexPath.row][@"post_image"];
        if (imageUrl.length == 0)
        {
            imageUrl = categoriesArray[indexPath.row][@"category_image"];
        }
    }
    
    [cell.categoryImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error)
        {
            [cell.categoryImageView sd_setImageWithURL:[NSURL URLWithString:categoriesArray[indexPath.row][@"category_image"]]];
        }
        else
        {
            cell.categoryImageView.image = image;
        }
    }];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 101) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(categorySearchViewDidSelectedCategoryWithName:andCategoryTag:)])
        {
            NSString *categoryName = [NSString stringWithFormat:@"#%@",trendingArray[indexPath.row]];
            NSString *categoryTag = trendingArray[indexPath.row];
            [self.delegate categorySearchViewDidSelectedCategoryWithName:categoryName andCategoryTag:categoryTag];
        }
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(categorySearchViewDidSelectedCategoryWithName:andCategoryTag:)])
    {
        NSString *categoryName = categoriesArray[indexPath.row][@"category_title"];
        NSString *categoryTag = categoriesArray[indexPath.row][@"category_tag"];
        [self.delegate categorySearchViewDidSelectedCategoryWithName:categoryName andCategoryTag:categoryTag];
    }
}


-(void)getCategoryList
{
    NSString *url = [NSString stringWithFormat:@"posts/getPopularPostIncategory"];
    
    [[KIHTTPRequestOperationManager manager]cancelAllHTTPOperationsWithPath:url];
    
    NSMutableDictionary *header =[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"application/json",@"Content-Type",[NSUSERDEFAULTS valueForKey:kUSERTOKEN],@"User-Token", nil];
        
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:header withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"--responce dict %@",responseDict);
        
        [CommonFunction removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunction alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([operation.response statusCode]==200)
        {
            if (responseDict[@"categories"] && [responseDict[@"categories"] isKindOfClass:[NSArray class]])
            {
                NSArray *categoryArray = responseDict[@"categories"];
                if (categoryArray.count>0)
                {
                    [categoriesArray removeAllObjects];
                    [categoriesArray addObjectsFromArray:[categoryArray mutableCopy]];
                    [self.collectionCategories reloadData];
                }
            }
            
            if (responseDict[@"hashtags"] && [responseDict[@"hashtags"] isKindOfClass:[NSArray class]])
            {
                NSArray *hashtagArray = responseDict[@"hashtags"];
                if (hashtagArray.count>0)
                {
                    self.labelEmptyHashtag.hidden = YES;
                    [trendingArray removeAllObjects];
                    [trendingArray addObjectsFromArray:[hashtagArray mutableCopy]];
                    [self.collectionHashTags reloadData];
                }
                else
                {
                    self.labelEmptyHashtag.hidden = NO;
                }
            }
        }
        else
        {
            [CommonFunction alertTitle:@"" withMessage:[responseDict objectForKey:@"msg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          [CommonFunction removeActivityIndicator];

                                          if (error.code == -999) {
                                              
                                          }
                                          else if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo response %@",operation.response);
                                              [CommonFunction alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              if (wbServiceCount==1) {
                                                  wbServiceCount=0;
                                              }
                                              else
                                              {
                                                  [CommonFunction alertTitle:@"" withMessage:@"Network Error..."];
                                              }
                                          }
                                          
                                      }];
}



@end
