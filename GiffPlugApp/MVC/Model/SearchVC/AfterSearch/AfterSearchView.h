//
//  AfterSearchView.h
//  GiffPlugApp
//
//  Created by Santosh on 02/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AfterSearchDelegate;

@interface AfterSearchView : UIView
@property (nonatomic,strong)NSString *searchText;
@property NSInteger currentPageRecent, totalPageRecent;
@property BOOL isPageRefreshingRecent;
@property (nonatomic,weak)id<AfterSearchDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *loaderFooter;
@property (weak, nonatomic) IBOutlet UIImageView *loaderImageView;
- (void)setSearchDataDictionary:(NSDictionary *)searchDictionary;
- (void)resetView;
@end

@protocol AfterSearchDelegate <NSObject>

- (void)afterSearchViewDidSelectedUserWithName:(NSString *)username;
- (void)afterSearchViewDidSelectedGifID:(NSString *)postID;
@end