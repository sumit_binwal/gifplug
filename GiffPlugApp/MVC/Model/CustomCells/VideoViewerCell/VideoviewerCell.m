//
//  ImageviewerCell.m
//  GiffPlugApp
//
//  Created by Santosh on 07/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "VideoviewerCell.h"

@implementation VideoviewerCell

- (void)awakeFromNib {
    // Initialization code
    _fileUrl = [NSString string];
    _imageData = [NSData new];
//    self.gifImageView.layer.cornerRadius = 10.0f;
    [self viewWithTag:101].layer.cornerRadius = 2.0f;
}

@end
