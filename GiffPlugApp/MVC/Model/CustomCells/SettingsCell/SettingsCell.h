//
//  SettingsCell.h
//  GiffPlugApp
//
//  Created by Santosh on 31/05/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    ICON_TYPE_ARROW,
    ICON_TYPE_SELECTED,
    ICON_TYPE_UNSELECTED
} ICON_TYPE;
static NSString *const kCellIdentifierHeader    =   @"headerCell";
static NSString *const kCellIdentifierContent    =   @"contentCell";
static NSString *const kCellIdentifierContentPrivate    =   @"contentCellPrivate";
@interface SettingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UIImageView *iconSelected;
@property (nonatomic)ICON_TYPE iconType;
@end
