//
//  PostedGifsCustomeCell.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 04/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "PostedGifsCustomeCell.h"

@interface PostedGifsCustomeCell ()
{
    BOOL isCaptionOpened;
}
@end

@implementation PostedGifsCustomeCell

- (void)awakeFromNib {
    // Initialization code

    __weak typeof(self) weakSelf = self;
    
    self.lblCaption.userHandleLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        if ([weakSelf.delegate respondsToSelector:@selector(customTableViewCellDidTapOnUserHandle:)]) {
            [weakSelf.delegate customTableViewCellDidTapOnUserHandle:[string substringFromIndex:1]];
        }
    };
    
    self.lblCaption.hashtagLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        if ([weakSelf.delegate respondsToSelector:@selector(customTableViewCellDidTapHashTagHandle:)]) {
            [weakSelf.delegate customTableViewCellDidTapHashTagHandle:[string substringFromIndex:1]];
        }
    };
    
//    UITapGestureRecognizer *captionTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onExpandCaptionView:)];
//    captionTap.numberOfTapsRequired=1;
//    [self.lblCaption addGestureRecognizer:captionTap];
    
    self.imgLike.alpha=0;
    self.imgLike.transform=CGAffineTransformMakeScale(0.5,0.5);
    isCaptionOpened = NO;
    _isHeartAnimationActive = NO;
}
- (void)setProgress:(float)progress {
    if (_progress != progress) {
        _progress = progress;
        
        NSLog(@"%f",_progress);
        NSLog(@"%f",progress);
        // Update View
        [self updateView];
    }
}
- (void)updateView {
    [self.progresView setProgress:_progress];
      NSLog(@"%f",_progress);
}
-(void)setGiffImage:(FLAnimatedImage *)giffImage
{
    
    if (giffImage) {
        
        [_imgVwGif setHidden:false];
        [_imgVwGif setAnimatedImage:giffImage];
    }
    
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    if (CGRectContainsPoint(_lblCaption.superview.frame, location))
    {
        [self onExpandCaptionView:nil];
    }
}

- (void)performHeartAnimation
{
    [UIView animateWithDuration:0.6 delay:0.0 usingSpringWithDamping:0.4  initialSpringVelocity:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _isHeartAnimationActive = YES;
        self.imgLike.alpha=1;
        self.imgLike.transform=CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            self.imgLike.transform=CGAffineTransformMakeScale(0.5,0.5);
            self.imgLike.alpha=0;
        } completion:^(BOOL finished) {
            _isHeartAnimationActive = NO;
        }];
    }];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)onExpandCaptionView:(id)sender {
    
    [self layoutIfNeeded];

//    if (self.bottomCaptionAspectconstraint.priority == 999)
//    {
//        self.bottomCaptionAspectconstraint.priority = 997;
////        self.bottomCaptionAspectconstraint.active = NO;
//        NSLog(@"%f",self.lblCaption.intrinsicContentSize.height);
//        if (self.lblCaption.intrinsicContentSize.height>21)
//        {
//            self.captionHeightConstraint.constant = self.lblCaption.intrinsicContentSize.height+8;
//        }
//        else
//        {
//            self.captionHeightConstraint.constant = 21+6;
//        }
//    }
//    else
//    {
//        self.bottomCaptionAspectconstraint.priority = 999;
////        self.bottomCaptionAspectconstraint.active = YES;
//        self.captionHeightConstraint.constant = 21;
//    }
    
    
    if (!isCaptionOpened && self.lblCaption.intrinsicContentSize.height>21)
    {
        isCaptionOpened = YES;
        self.captionHeightConstraint.constant = self.lblCaption.intrinsicContentSize.height+8;
    }
    else
    {
        isCaptionOpened = NO;
        self.captionHeightConstraint.constant = 21;
    }

    [UIView animateWithDuration:0.1 delay:0 options:7 animations:^{
        [self layoutIfNeeded];
    } completion:nil];

//    if (self.lblCaption.numberOfLines == 1)
//    {
//        self.lblCaption.numberOfLines = 0;
//    }
//    else
//    {
//        self.lblCaption.numberOfLines = 1;
//    }
//
}


- (void)resetCaptionBar
{
    if (_lblCaption.text.length>0)
    {
        isCaptionOpened = NO;
        self.captionHeightConstraint.constant = 21;
        self.captionTopConstraint.constant = 8;
        self.captionBottomtConstraint.constant = 8;
        self.captionContainerHeightConstraint.constant = 37;
    }
}

- (void)handleCaptionBar
{
    if (_lblCaption.text.length==0) {
        self.captionHeightConstraint.constant = 0;
        self.captionTopConstraint.constant = 0;
        self.captionBottomtConstraint.constant = 0;
        self.captionContainerHeightConstraint.constant = 0;
    }
    else
    {
        [self resetCaptionBar];
    }
}

@end
