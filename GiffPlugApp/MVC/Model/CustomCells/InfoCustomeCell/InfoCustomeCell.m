//
//  FollowUnfollowCustomeCell.m
//  GiffPlugApp
//
//  Created by Sumit Sharma on 29/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "InfoCustomeCell.h"
static NSString *kExpansionToken = @"...Read More";
static NSString *kCollapseToken = @"Read Less";
@implementation InfoCustomeCell


- (void)awakeFromNib
{
    [self configureCellForFirstUse];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configureCellForFirstUse
{
    self.loader.hidden=YES;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
//    self.imgProfileImg.layer.cornerRadius=self.imgProfileImg.frame.size.width/2*SCREEN_XScale;
//    self.imgProfileImg.layer.borderColor=[UIColor colorWithRed:167.0f/255.0f green:167.0f/255.0f blue:167.0f/255.0f alpha:1].CGColor;
//    self.imgProfileImg.layer.borderWidth=1.0f;
//    self.imgProfileImg.clipsToBounds=YES;
    
    self.imageViewProfile.layer.cornerRadius=self.imageViewProfile.frame.size.width/2*SCREEN_XScale;
    self.imageViewProfile.layer.borderColor=[UIColor colorWithRed:167.0f/255.0f green:167.0f/255.0f blue:167.0f/255.0f alpha:1].CGColor;
    self.imageViewProfile.layer.borderWidth=1.0f;
    self.imageViewProfile.clipsToBounds=YES;
    
    CGFloat fontSizeBio = self.lblUserBio.font.pointSize;
    CGFloat fontSizeUser = self.lblUsername.font.pointSize;
    
    [self.lblUsername setFont:[UIFont fontWithName:self.lblUsername.font.fontName size:fontSizeUser*SCREEN_XScale]];
    [self.lblUserBio setFont:[UIFont fontWithName:self.lblUserBio.font.fontName size:fontSizeBio*SCREEN_XScale]];
    
    [self.lblTime setFont:[UIFont fontWithName:self.lblTime.font.fontName size:(self.lblTime.font.pointSize*SCREEN_XScale)]];
}

#pragma mark -

@end
