//
//  FollowUnfollowCustomeCell.h
//  GiffPlugApp
//
//  Created by Sumit Sharma on 29/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResponsiveLabel.h"

@protocol CustomTableViewCellDelegate;
@interface InfoCustomeCell : UITableViewCell
@property (nonatomic, weak) id<CustomTableViewCellDelegate>delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblComment;

@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UIButton *btnFollowUnfollow;
@property (strong, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UIButton *imgProfileImg;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UILabel *labelRequested;
@property (strong, nonatomic) IBOutlet ResponsiveLabel *lblUserBio;
//- (void)configureText:(NSString*)str forExpandedState:(BOOL)isExpanded;
@end
