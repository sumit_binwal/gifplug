//
//  RecentSearchCell.m
//  GiffPlugApp
//
//  Created by Santosh on 28/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import "RecentSearchCell.h"

@interface RecentSearchCell ()

@property (weak, nonatomic) IBOutlet UILabel *userName;
@end
@implementation RecentSearchCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateNameWithText:(NSString *)usernameText
{
    self.userName.text = usernameText;
}

- (NSString *)getRecentSearchText
{
    return self.userName.text;
}

@end
