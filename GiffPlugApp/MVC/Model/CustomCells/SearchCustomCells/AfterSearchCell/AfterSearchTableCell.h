//
//  AfterSearchTableCell.h
//  GiffPlugApp
//
//  Created by Santosh on 02/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum : NSUInteger {
    CELL_TYPE_LABEL,
    CELL_TYPE_COLLECTION_GIF,
    CELL_TYPE_COLLECTION_USERS,
    CELL_TYPE_COLLECTION_GIF_RECENT
} CELLTYPE;
static NSString *const kCellAfterSearchIdentifier = @"kCellAfterSearchIdentifier";
static NSString *const kCellAfterSearchIdentifierLabel = @"kCellAfterSearchIdentifierLabel";
static NSString *const kCellAfterSearchIdentifierGif = @"kCellAfterSearchIdentifierGif";
static NSString *const kCellAfterSearchIdentifierGifRecent = @"kCellAfterSearchIdentifierGifRecent";
@interface AfterSearchTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *labelHeader;
@property (assign)CELLTYPE cellType;
@property BOOL isRegistered;

- (void)adjustViewAccordingToCellType;

@end
