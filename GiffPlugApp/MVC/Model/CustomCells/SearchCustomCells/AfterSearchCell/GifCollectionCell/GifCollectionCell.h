//
//  GifCollectionCell.h
//  GiffPlugApp
//
//  Created by Santosh on 02/04/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"
static NSString *const kCellGifCollectionIdentifier = @"kCellGifCollectionIdentifier";

@interface GifCollectionCell : UICollectionViewCell
@property (strong,nonatomic)UIImage *singleImage;
@property (strong,nonatomic)UIImage *animatingImage;
@property (weak, nonatomic) IBOutlet FLAnimatedImageView *gifImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property BOOL isViewAnimating;

- (void)notifyCellVisibleWithIsCompletelyVisible:(BOOL)isVisible;

@end
