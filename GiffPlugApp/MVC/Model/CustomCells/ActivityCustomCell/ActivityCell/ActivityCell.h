//
//  ActivityCell.h
//  GiffPlugApp
//
//  Created by Santosh on 31/03/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kCellIdentifierReplug    =   @"replugCell";
static NSString *const kCellIdentifierLike    =   @"likeCell";
static NSString *const kCellIdentifierComment    =   @"commentCell";
static NSString *const kCellIdentifierFollow    =   @"followCell";

@interface ActivityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *activityGifView;
@property (weak, nonatomic) IBOutlet UIImageView *activityProfileView;

@property (weak, nonatomic) IBOutlet UILabel *activityUserName;
@property (weak, nonatomic) IBOutlet UILabel *activityTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityTimeStampLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelRequested;
@property (weak, nonatomic) IBOutlet UIImageView *activityIcon;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIButton *activityFollowUnFollowBtn;
@end
