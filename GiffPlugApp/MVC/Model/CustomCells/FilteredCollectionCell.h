//
//  FilteredCollectionCell.h
//  GiffPlugApp
//
//  Created by Kshitij Godara on 12/01/16.
//  Copyright © 2016 Kshitij Godara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilteredCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *filterLabel;
@property (weak, nonatomic) IBOutlet UIView *smallIndicatorView;
@property (weak, nonatomic) IBOutlet UIImageView *filteredImageView;
@property (weak, nonatomic) IBOutlet UIView *bottomIndicatorBar;

@end
