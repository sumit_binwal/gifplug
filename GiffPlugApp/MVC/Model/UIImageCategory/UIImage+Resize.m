//
//  UIImage+Resize.m
//
//  Created by Olivier Halligon on 12/08/09.
//  Copyright 2009 AliSoftware. All rights reserved.
//

#import "UIImage+Resize.h"

@implementation UIImage (ResizeCategory)

-(UIImage*)resizedImageToSize:(CGSize)dstSize
{
	CGImageRef imgRef = self.CGImage;
	// the below values are regardless of orientation : for UIImages from Camera, width>height (landscape)
	CGSize  srcSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef)); // not equivalent to self.size (which is dependant on the imageOrientation)!
	
    /* Don't resize if we already meet the required destination size. */
    if (CGSizeEqualToSize(srcSize, dstSize)) {
        return self;
    }
    
	CGFloat scaleRatio = dstSize.width / srcSize.width;
	UIImageOrientation orient = self.imageOrientation;
	CGAffineTransform transform = CGAffineTransformIdentity;
	switch(orient) {
			
		case UIImageOrientationUp: //EXIF = 1
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
			transform = CGAffineTransformMakeTranslation(srcSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
			transform = CGAffineTransformMakeTranslation(srcSize.width, srcSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
			transform = CGAffineTransformMakeTranslation(0.0, srcSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
			dstSize = CGSizeMake(dstSize.height, dstSize.width);
			transform = CGAffineTransformMakeTranslation(srcSize.height, srcSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI_2);
			break;  
			
		case UIImageOrientationLeft: //EXIF = 6  
			dstSize = CGSizeMake(dstSize.height, dstSize.width);
			transform = CGAffineTransformMakeTranslation(0.0, srcSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI_2);
			break;  
			
		case UIImageOrientationRightMirrored: //EXIF = 7  
			dstSize = CGSizeMake(dstSize.height, dstSize.width);
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
			break;  
			
		case UIImageOrientationRight: //EXIF = 8  
			dstSize = CGSizeMake(dstSize.height, dstSize.width);
			transform = CGAffineTransformMakeTranslation(srcSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
			break;  
			
		default:  
			[NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];  
			
	}  
	
	/////////////////////////////////////////////////////////////////////////////
	// The actual resize: draw the image on a new context, applying a transform matrix
	UIGraphicsBeginImageContextWithOptions(dstSize, NO, 1.0);
	
	CGContextRef context = UIGraphicsGetCurrentContext();
    
       if (!context) {
           return nil;
       }
	
	if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
		CGContextScaleCTM(context, -scaleRatio, scaleRatio);
		CGContextTranslateCTM(context, -srcSize.height, 0);
	} else {  
		CGContextScaleCTM(context, scaleRatio, -scaleRatio);
		CGContextTranslateCTM(context, 0, -srcSize.height);
	}
	
	CGContextConcatCTM(context, transform);
	
	// we use srcSize (and not dstSize) as the size to specify is in user space (and we use the CTM to apply a scaleRatio)
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, srcSize.width, srcSize.height), imgRef);
	UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return resizedImage;
}



/////////////////////////////////////////////////////////////////////////////



-(UIImage*)resizedImageToFitInSize:(CGSize)boundingSize scaleIfSmaller:(BOOL)scale
{
	// get the image size (independant of imageOrientation)
	CGImageRef imgRef = self.CGImage;
	CGSize srcSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef)); // not equivalent to self.size (which depends on the imageOrientation)!

	// adjust boundingSize to make it independant on imageOrientation too for farther computations
	UIImageOrientation orient = self.imageOrientation;  
	switch (orient) {
		case UIImageOrientationLeft:
		case UIImageOrientationRight:
		case UIImageOrientationLeftMirrored:
		case UIImageOrientationRightMirrored:
			boundingSize = CGSizeMake(boundingSize.height, boundingSize.width);
			break;
        default:
            // NOP
            break;
	}

	// Compute the target CGRect in order to keep aspect-ratio
	CGSize dstSize;
	
	if ( !scale && (srcSize.width < boundingSize.width) && (srcSize.height < boundingSize.height) ) {
		//NSLog(@"Image is smaller, and we asked not to scale it in this case (scaleIfSmaller:NO)");
		dstSize = srcSize; // no resize (we could directly return 'self' here, but we draw the image anyway to take image orientation into account)
	} else {		
		CGFloat wRatio = boundingSize.width / srcSize.width;
		CGFloat hRatio = boundingSize.height / srcSize.height;
		
		if (wRatio < hRatio) {
			//NSLog(@"Width imposed, Height scaled ; ratio = %f",wRatio);
			dstSize = CGSizeMake(boundingSize.width, floorf(srcSize.height * wRatio));
		} else {
			//NSLog(@"Height imposed, Width scaled ; ratio = %f",hRatio);
			dstSize = CGSizeMake(floorf(srcSize.width * hRatio), boundingSize.height);
		}
	}
		
	return [self resizedImageToSize:dstSize];
}

//- (instancetype)initWithAnimatedGifData:(NSData *)data
//{
//    // Early return if no data supplied!
//    BOOL hasData = ([data length] > 0);
//    if (!hasData) {
//        return nil;
//    }
//    
//    self = [super init];
//    if (self)
//    {
//        CGImageSourceRef imageSource = CGImageSourceCreateWithData((__bridge CFDataRef)data,
//                                                                   (__bridge CFDictionaryRef)@{(NSString *)kCGImageSourceShouldCache: @NO});
//        // Early return on failure!
//        if (!imageSource) {
//            return nil;
//        }
//        
//        // Early return if not GIF!
//        CFStringRef imageSourceContainerType = CGImageSourceGetType(imageSource);
//        BOOL isGIFData = UTTypeConformsTo(imageSourceContainerType, kUTTypeGIF);
//        if (!isGIFData) {
//            return nil;
//        }
//        
//        NSDictionary *imageProperties = (__bridge_transfer NSDictionary *)CGImageSourceCopyProperties(imageSource, NULL);
//        NSUInteger loopCount = [[[imageProperties objectForKey:(id)kCGImagePropertyGIFDictionary] objectForKey:(id)kCGImagePropertyGIFLoopCount] unsignedIntegerValue];
//        
//        // Iterate through frame images
//        size_t imageCount = CGImageSourceGetCount(imageSource);
//        NSUInteger skippedFrameCount = 0;
//        NSMutableDictionary *delayTimesForIndexesMutable = [NSMutableDictionary dictionaryWithCapacity:imageCount];
//        
//        
//        for (size_t i = 0; i < imageCount; i++) {
//            @autoreleasepool {
//                CGImageRef frameImageRef = CGImageSourceCreateImageAtIndex(imageSource, i, NULL);
//                if (frameImageRef) {
//                    UIImage *frameImage = [UIImage imageWithCGImage:frameImageRef];
//                    frameImage = [frameImage resizedImageToFitInSize:CGSizeMake(frameImage.size.width/4, frameImage.size.height/4) scaleIfSmaller:NO];
//                    // Check for valid `frameImage` before parsing its properties as frames can be corrupted (and `frameImage` even `nil` when `frameImageRef` was valid).
//                    if (frameImage) {
////                        // Set poster image
////                        if (!self.posterImage) {
////                            _posterImage = frameImage;
////                            // Set its size to proxy our size.
////                            _size = _posterImage.size;
////                            // Remember index of poster image so we never purge it; also add it to the cache.
////                            _posterImageFrameIndex = i;
////                            [self.cachedFramesForIndexes setObject:self.posterImage forKey:@(self.posterImageFrameIndex)];
////                            [self.cachedFrameIndexes addIndex:self.posterImageFrameIndex];
////                        }
//                        
//                        // Get `DelayTime`
//                        // Note: It's not in (1/100) of a second like still falsely described in the documentation as per iOS 8 (rdar://19507384) but in seconds stored as `kCFNumberFloat32Type`.
//                        // Frame properties example:
//                        // {
//                        //     ColorModel = RGB;
//                        //     Depth = 8;
//                        //     PixelHeight = 960;
//                        //     PixelWidth = 640;
//                        //     "{GIF}" = {
//                        //         DelayTime = "0.4";
//                        //         UnclampedDelayTime = "0.4";
//                        //     };
//                        // }
//                        
//                        NSDictionary *frameProperties = (__bridge_transfer NSDictionary *)CGImageSourceCopyPropertiesAtIndex(imageSource, i, NULL);
//                        NSDictionary *framePropertiesGIF = [frameProperties objectForKey:(id)kCGImagePropertyGIFDictionary];
//                        
//                        // Try to use the unclamped delay time; fall back to the normal delay time.
//                        NSNumber *delayTime = [framePropertiesGIF objectForKey:(id)kCGImagePropertyGIFUnclampedDelayTime];
//                        if (!delayTime) {
//                            delayTime = [framePropertiesGIF objectForKey:(id)kCGImagePropertyGIFDelayTime];
//                        }
//                        // If we don't get a delay time from the properties, fall back to `kDelayTimeIntervalDefault` or carry over the preceding frame's value.
//                        const NSTimeInterval kDelayTimeIntervalDefault = 0.1;
//                        if (!delayTime) {
//                            if (i == 0) {
//                                delayTime = @(kDelayTimeIntervalDefault);
//                            } else {
//                                delayTime = delayTimesForIndexesMutable[@(i - 1)];
//                            }
//                        }
//                        // Support frame delays as low as `kFLAnimatedImageDelayTimeIntervalMinimum`, with anything below being rounded up to `kDelayTimeIntervalDefault` for legacy compatibility.
//                        // To support the minimum even when rounding errors occur, use an epsilon when comparing. We downcast to float because that's what we get for delayTime from ImageIO.
//                        if ([delayTime floatValue] < ((float)0.02 - FLT_EPSILON)) {
//                            delayTime = @(kDelayTimeIntervalDefault);
//                        }
//                        delayTimesForIndexesMutable[@(i)] = delayTime;
//                    } else {
//                        skippedFrameCount++;
//                    }
//                    CFRelease(frameImageRef);
//                } else {
//                    skippedFrameCount++;
//                }
//            }
//        }
//        
//        NSDictionary *delayTimesForIndexes = [delayTimesForIndexesMutable copy];
//        NSUInteger frameCount = imageCount;
//        
//        if (frameCount == 0) {
//            return nil;
//        } else if (frameCount == 1) {
//            // Warn when we only have a single frame but return a valid GIF.
//        } else {
//            // We have multiple frames, rock on!
//        }
//        
//
//            // Calculate the optimal frame cache size: try choosing a larger buffer window depending on the predicted image size.
//            // It's only dependent on the image size & number of frames and never changes.
//            CGFloat animatedImageDataSize = CGImageGetBytesPerRow(self.posterImage.CGImage) * self.size.height * (frameCount - skippedFrameCount) / MEGABYTE;
//            if (animatedImageDataSize <= FLAnimatedImageDataSizeCategoryAll) {
//                _frameCacheSizeOptimal = self.frameCount;
//            } else if (animatedImageDataSize <= FLAnimatedImageDataSizeCategoryDefault) {
//                // This value doesn't depend on device memory much because if we're not keeping all frames in memory we will always be decoding 1 frame up ahead per 1 frame that gets played and at this point we might as well just keep a small buffer just large enough to keep from running out of frames.
//                _frameCacheSizeOptimal = FLAnimatedImageFrameCacheSizeDefault;
//            } else {
//                // The predicted size exceeds the limits to build up a cache and we go into low memory mode from the beginning.
//                _frameCacheSizeOptimal = FLAnimatedImageFrameCacheSizeLowMemory;
//            }
//        
//    }
//    return self;
//}
//+ (UIImage *)animatedImageWithData:(NSData *)data
//{
//    UIImage *animatedImage = [[UIImage alloc]initWithAnimatedGifData:data];
//    return animatedImage;
//}

@end
